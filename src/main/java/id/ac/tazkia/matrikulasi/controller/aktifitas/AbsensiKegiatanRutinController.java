package id.ac.tazkia.matrikulasi.controller.aktifitas;

import id.ac.tazkia.matrikulasi.dao.*;
import id.ac.tazkia.matrikulasi.entity.*;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.validation.Valid;
import java.util.*;

@Controller
public class AbsensiKegiatanRutinController {

    private @Autowired KaryawanDao karyawanDao;
    private @Autowired PlotingPembinaDao plotingPembinaDao;
    private @Autowired PenjadwalanKegiatanRutinDao penjadwalanKegiatanRutinDao;
    private @Autowired PresensiKegiatanDao presensiKegiatanDao;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("aktifitas/absensi_kegiatan_rutin")
    private String absensiKegiatanRutin(ModelMap mm, Pageable pageable) {

        Page<PenjadwalanKegiatanRutin> listKegiatan = penjadwalanKegiatanRutinDao.findByStatus(StatusRecord.AKTIF, pageable);


        if (!listKegiatan.isEmpty()) {
            for (PenjadwalanKegiatanRutin penjadwalan : listKegiatan.getContent()) {
                Set<Karyawan> karyawans = new HashSet<>();
                List<PresensiKegiatanRutin> listPresensi = presensiKegiatanDao.findByStatusAndPenjadwalanKegiatanRutinId(StatusRecord.AKTIF, penjadwalan.getId());
                for (PresensiKegiatanRutin presensi : listPresensi) {
                    if (presensi.getPenjadwalanKegiatanRutin().getId().equals(penjadwalan.getId()) && !StringUtils.isEmpty(presensi.getKaryawan())) {
                        karyawans.add(presensi.getKaryawan());
                        penjadwalan.setPembinas(karyawans);
                    }
                }
            }
        }

        mm.addAttribute("listKegiatan", listKegiatan);

        return "aktifitas/absensi_kegiatan_rutin/list";
    }


    @GetMapping("aktifitas/absensi_kegiatan_rutin/absensi_mahasiswa")
    private String absensiKegiatanMahasiswa(@RequestParam(required = false) String detail,
                                            @RequestParam(required = false) String pembina,
                                            ModelMap mm, @PageableDefault Pageable pageable) {

        Page<PlotingPembina> pagePloting = plotingPembinaDao.findByStatusAndPembinaId(StatusRecord.AKTIF, pembina, pageable);
        Page<PlotingPembina> plotingPembinas = plotingPembinaDao.findByStatusAndMahasiswaStatusAndPembinaStatusOrderByMahasiswaNimAsc(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.AKTIF, pageable);
        PenjadwalanKegiatanRutin penjadwalan = penjadwalanKegiatanRutinDao.findById(detail).get();
        List<Mahasiswa> mahasiswas = new ArrayList<>();

        for (PlotingPembina plotingPembina : plotingPembinas.getContent()) {
            mahasiswas.add(plotingPembina.getMahasiswa());
        }

        for (Mahasiswa mahasiswa : mahasiswas) {
            mahasiswa.setStatusPresensi(penjadwalan.getMahasiswaPresensi(mahasiswa));
        }

        mm.addAttribute("penjadwalan", penjadwalan);
        mm.addAttribute("listPlotingMahasiswa", pagePloting);

        return "aktifitas/absensi_kegiatan_rutin/absensi_mahasiswa/list";
    }


    @GetMapping("aktifitas/berita_acara/new")
    private String beritaAcaraForm(@RequestParam(required = false) String detail,
                                   ModelMap mm, @PageableDefault Pageable pageable) {

        PenjadwalanKegiatanRutin penjadwalanKegiatanRutin = penjadwalanKegiatanRutinDao.findById(detail).get();


        mm.addAttribute("penjadwalanKegiatanRutin", penjadwalanKegiatanRutin);
        mm.addAttribute("pembina", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, pageable));
        mm.addAttribute("presensiKegiatanRutin", new PresensiKegiatanRutin());

        return "aktifitas/absensi_kegiatan_rutin/berita_acara/form";
    }

    @PostMapping("aktifitas/berita_acara/save")
    private String prosesBeritaAcara(@RequestParam(required = false) String detail,
                                     @ModelAttribute @Valid PresensiKegiatanRutin presensi, Pageable pageable,
                                     BindingResult errors, ModelMap mm, RedirectAttributes redir) {

        PenjadwalanKegiatanRutin penjadwalanKegiatanRutin = penjadwalanKegiatanRutinDao.findById(detail).get();
        if (errors.hasErrors()) {
            mm.addAttribute("penjadwalanKegiatanRutin", penjadwalanKegiatanRutin);
            mm.addAttribute("karyawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, pageable));
            mm.addAttribute("penjadwalanKegiatanRutin", penjadwalanKegiatanRutin);
            mm.addAttribute("presensiKegiatan", presensi);
            return "aktifitas/absensi_kegiatan_rutin/berita_acara/form";
        }

        PresensiKegiatanRutin dupeKaryawan = presensiKegiatanDao.findByStatusAndKaryawanIdAndPenjadwalanKegiatanRutinId(StatusRecord.AKTIF, presensi.getKaryawan().getId(), penjadwalanKegiatanRutin.getId());
        if (dupeKaryawan != null && !dupeKaryawan.getId().equalsIgnoreCase(presensi.getId())) {
            mm.addAttribute("pembina", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, pageable));
            mm.addAttribute("error", "Pembina Yang Dipilih Sudah Ada");
            mm.addAttribute("penjadwalanKegiatanRutin", penjadwalanKegiatanRutin);
            mm.addAttribute("presensiKegiatan", presensi);
            return "aktifitas/absensi_kegiatan_rutin/berita_acara/form";
        }

        presensi.setPenjadwalanKegiatanRutin(penjadwalanKegiatanRutin);
        presensiKegiatanDao.save(presensi);

        redir.addFlashAttribute("success", "Save data successfully");

        return "redirect:/aktifitas/absensi_kegiatan_rutin";
    }


    @PostMapping("aktifitas/absensi_kegiatan_rutin/save")
    private String prosesabsensiKegiatanRutin(@ModelAttribute("penjadwalan") PenjadwalanKegiatanRutin penjadwalan, ModelMap mm, RedirectAttributes redir) {

        PenjadwalanKegiatanRutin penjadwalanKegiatanRutin = penjadwalanKegiatanRutinDao.findById(penjadwalan.getId()).get();
        penjadwalanKegiatanRutin.getPresensiMahasiswaKegiatanRutins().clear();
        penjadwalanKegiatanRutin.getPresensiMahasiswaKegiatanRutins().addAll(penjadwalan.getPresensiMahasiswaKegiatanRutins());

        penjadwalanKegiatanRutinDao.save(penjadwalanKegiatanRutin);
        redir.addFlashAttribute("success", "Kehadiran Tersimpan");

//        mm.addAttribute("listKegiatan", penjadwalanKegiatanRutinDao.listKegiatan(pageable));
        return "redirect:/aktifitas/absensi_kegiatan_rutin";
    }

}
