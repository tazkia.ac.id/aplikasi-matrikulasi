package id.ac.tazkia.matrikulasi.controller.aktifitas;

import id.ac.tazkia.matrikulasi.dao.*;
import id.ac.tazkia.matrikulasi.dto.BeritaAcaraDto;
import id.ac.tazkia.matrikulasi.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class AbsensiKegiatanTemporerController {

    @Autowired
    private PresensiDosenKegiatanTemporerDao presensiDosenKegiatanTemporerDao;

    @Autowired
    private PresensiMahasiswaKegiatanTemporerDao presensiMahasiswaKegiatanTemporerDao;

    @Autowired
    private MahasiswaDao mahasiswaDao;

    @Autowired
    private PlotingPembinaDao plotingPembinaDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private PenjadwalanKegiatanTemporerDao temporerDao;

    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("aktifitas/absensi_temporer")
    private String absenTemporer(ModelMap model, @PageableDefault Pageable pageable) {

        Page<PenjadwalanKegiatanTemporer> listKegiatan = temporerDao.findByStatus(StatusRecord.AKTIF, pageable);
        if (!listKegiatan.isEmpty()) {
            for (PenjadwalanKegiatanTemporer temporer : listKegiatan.getContent()) {
                Set<Karyawan> karyawans = new HashSet<>();
                List<PresensiDosenKegiatanTemporer> listTemporer = presensiDosenKegiatanTemporerDao.findByStatusAndKegiatanTemporerId(StatusRecord.AKTIF, temporer.getId());
                for (PresensiDosenKegiatanTemporer kegiatanTemporer : listTemporer) {
                    if (kegiatanTemporer.getKegiatanTemporer().getId().equals(temporer.getId())) {
                        karyawans.add(kegiatanTemporer.getKaryawan());
                        temporer.setPembina(karyawans);
                    }
                }
            }
        }

        model.addAttribute("listKegiatanTemporer", listKegiatan);
        return "aktifitas/absensi_temporer/list";
    }

    @GetMapping("aktifitas/absensi_temporer/berita_acara/new")
    private String beritaAcara(@RequestParam(required = false) String detail,
                               ModelMap modelMap, Pageable pageable) {

        PenjadwalanKegiatanTemporer kegiatanTemporer = temporerDao.findById(detail).get();

        modelMap.addAttribute("kegiatanTemporer", kegiatanTemporer);
        modelMap.addAttribute("pembina", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, pageable));
        modelMap.addAttribute("presensiDosen", new PresensiDosenKegiatanTemporer());

        return "aktifitas/absensi_temporer/berita_acara/form";
    }

    @PostMapping("aktifitas/absensi_temporer/berita_acara/save")
    private String formBeritaAcara(@RequestParam(required = false) String detail,
                                   @ModelAttribute @Valid PresensiDosenKegiatanTemporer temporer, BindingResult errors,
                                   Pageable pageable, ModelMap modelMap, RedirectAttributes attributes) {

        PenjadwalanKegiatanTemporer penjadwalanKegiatanTemporer = temporerDao.findById(detail).get();
        if (errors.hasErrors()) {
            modelMap.addAttribute("pembina", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, pageable));
            modelMap.addAttribute("presensiDosen", temporer);
            return "aktifitas/absensi_temporer/berita_acara/form";
        }

        PresensiDosenKegiatanTemporer dupeCode = presensiDosenKegiatanTemporerDao.findByStatusAndKaryawanIdAndKegiatanTemporerId(StatusRecord.AKTIF, temporer.getKaryawan().getId() ,penjadwalanKegiatanTemporer.getId());
        if (dupeCode != null && !dupeCode.getId().equalsIgnoreCase(temporer.getId())) {
            modelMap.addAttribute("error", "Dosen sudah di pilih!");
            modelMap.addAttribute("pembina", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, pageable));
            modelMap.addAttribute("kegiatanTemporer", penjadwalanKegiatanTemporer);
            modelMap.addAttribute("presensiDosen", temporer);
            return "aktifitas/absensi_temporer/berita_acara/form";
        }

        temporer.setKegiatanTemporer(penjadwalanKegiatanTemporer);
        presensiDosenKegiatanTemporerDao.save(temporer);

        attributes.addFlashAttribute("success", "Save data successfully");
        return "redirect:/aktifitas/absensi_temporer";
    }

    @GetMapping("aktifitas/absensi_temporer/absensi_mahasiswa")
    private String listAbsensiMahasiswa(@RequestParam(required = false) String detail,
                                        @RequestParam(required = false) String pembina,
                                        ModelMap modelMap, @PageableDefault Pageable pageable) {

        Page<PlotingPembina> pagePloting = plotingPembinaDao.findByStatusAndPembinaId(StatusRecord.AKTIF, pembina, pageable);
        Page<PlotingPembina> plotingPembinas = plotingPembinaDao.findByStatusAndMahasiswaStatusAndPembinaStatusOrderByMahasiswaNimAsc(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.AKTIF, pageable);
        PenjadwalanKegiatanTemporer penjadwalan = temporerDao.findById(detail).get();
        List<Mahasiswa> mahasiswas = new ArrayList<>();

        for (PlotingPembina plotingPembina : plotingPembinas) {
            mahasiswas.add(plotingPembina.getMahasiswa());
        }

        for (Mahasiswa mahasiswa : mahasiswas) {
            mahasiswa.setStatusPresensi(penjadwalan.getMahasiswaAttendance(mahasiswa));
        }

        modelMap.addAttribute("penjadwalan", penjadwalan);
        modelMap.addAttribute("listPlotingMahasiswa", pagePloting);

        return "aktifitas/absensi_temporer/absensi_mahasiswa/list";
    }

    @PostMapping("aktifitas/absensi_temporer/save")
    private String processAbsensiMahasiswa(@ModelAttribute("kegiatanTemporer") PenjadwalanKegiatanTemporer kegiatanTemporer,
                                           String detail, RedirectAttributes attributes, ModelMap modelMap) {

        PenjadwalanKegiatanTemporer kegiatanTemporerObj = temporerDao.findById(kegiatanTemporer.getId()).get();
        kegiatanTemporerObj.getAbsenMahasiswas().clear();
        kegiatanTemporerObj.getAbsenMahasiswas().addAll(kegiatanTemporer.getAbsenMahasiswas());

        temporerDao.save(kegiatanTemporerObj);
        attributes.addFlashAttribute("success", "Absensi berhasil di simpan!");
        return "redirect:/aktifitas/absensi_temporer";
    }
}