package id.ac.tazkia.matrikulasi.controller.aktifitas;

import id.ac.tazkia.matrikulasi.dao.AgamaDao;
import id.ac.tazkia.matrikulasi.dao.KegiatanDao;
import id.ac.tazkia.matrikulasi.dao.PenjadwalanKegiatanRutinDao;
import id.ac.tazkia.matrikulasi.dao.RuanganDao;
import id.ac.tazkia.matrikulasi.entity.Karyawan;
import id.ac.tazkia.matrikulasi.entity.PenjadwalanKegiatanRutin;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class PenjadwalanKegiatanRutinController {

    @Autowired PenjadwalanKegiatanRutinDao penjadwalanKegiatanRutinDao;
    @Autowired KegiatanDao kegiatanDao;
    @Autowired RuanganDao ruanganDao;

    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    @GetMapping("/aktifitas/penjadwalan_kegiatan_rutin")
    private String penjadwalanKegiatanRutin(@RequestParam(required = false) String search, ModelMap mm, @PageableDefault(size = 10) Pageable pageable) {

        if (StringUtils.hasText(search)) {
            mm.addAttribute("penjadwalan", penjadwalanKegiatanRutinDao.findByStatusAndSearch(search, StatusRecord.AKTIF, pageable));
        } else {
            mm.addAttribute("penjadwalan", penjadwalanKegiatanRutinDao.findByStatus(StatusRecord.AKTIF, pageable));
        }

        return "aktifitas/penjadwalan_kegiatan_rutin/list";
    }

    @GetMapping("/aktifitas/penjadwalan_kegiatan_rutin/new")
    private String penjadwalanKegiatanRutinNew(@RequestParam(required = false) String id, ModelMap mm, @PageableDefault
                                               Pageable pageable) {

        if (StringUtils.hasText(id)) {
            mm.addAttribute("penjadwalan", penjadwalanKegiatanRutinDao.findById(id).get());
        } else {
            mm.addAttribute("penjadwalan", new PenjadwalanKegiatanRutin());
        }

        mm.addAttribute("kegiatan", kegiatanDao.findByStatusOrderByNamaKegiatanAsc(StatusRecord.AKTIF, pageable));
        mm.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
        return "aktifitas/penjadwalan_kegiatan_rutin/form";
    }

    @PostMapping("/aktifitas/penjadwalan_kegiatan_rutin/save")
    private String penjadwalanKegiatanRutinSave(@Valid PenjadwalanKegiatanRutin penjadwalanKegiatanRutin,
                                                BindingResult errors, @RequestParam(required = false)String jamMulai
                                                , RedirectAttributes attributes, ModelMap mm, Pageable pageable) {

        if (errors.hasErrors()) {
            mm.addAttribute("penjadwalan", penjadwalanKegiatanRutin);
            mm.addAttribute("kegiatan", kegiatanDao.findByStatusOrderByNamaKegiatanAsc(StatusRecord.AKTIF, pageable));
            mm.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
            return "aktifitas/penjadwalan_kegiatan_rutin/form";
        }

        PenjadwalanKegiatanRutin penjadwalan = penjadwalanKegiatanRutinDao.findByJamMulai(penjadwalanKegiatanRutin.getJamMulai());
        if (penjadwalan != null && !penjadwalan.getId().equalsIgnoreCase(penjadwalanKegiatanRutin.getId())){
            mm.addAttribute("error", "Jam tersebut telah digunakan" );
            mm.addAttribute("penjadwalan", penjadwalanKegiatanRutin);
            mm.addAttribute("kegiatan", kegiatanDao.findByStatusOrderByNamaKegiatanAsc(StatusRecord.AKTIF, pageable));
            mm.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
            return "aktifitas/penjadwalan_kegiatan_rutin/form";
        }

        String date = penjadwalanKegiatanRutin.getTanggalPelaksanaanString();
        String tahun = date.substring(6, 10);
        String bulan = date.substring(0, 2);
        String tanggal = date.substring(3, 5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        penjadwalanKegiatanRutin.setTanggalPelaksanaan(localDate.plusDays(0));

        penjadwalanKegiatanRutinDao.save(penjadwalanKegiatanRutin);
        attributes.addFlashAttribute("success", "Save Data Berhasil");

        return "redirect:/aktifitas/penjadwalan_kegiatan_rutin";
    }

    @PostMapping("/aktifitas/penjadwalan_kegiatan_rutin/hapus")
    public String deleteEmployee(@RequestParam(required = false)String id,
                                 RedirectAttributes attributes){

        PenjadwalanKegiatanRutin penjadwalan = penjadwalanKegiatanRutinDao.findById(id).get();

        penjadwalan.setStatus(StatusRecord.HAPUS);
        penjadwalanKegiatanRutinDao.save(penjadwalan);

        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/aktifitas/penjadwalan_kegiatan_rutin";

    }
}