package id.ac.tazkia.matrikulasi.controller.aktifitas;

import id.ac.tazkia.matrikulasi.dao.KegiatanDao;
import id.ac.tazkia.matrikulasi.dao.PenjadwalanKegiatanTemporerDao;
import id.ac.tazkia.matrikulasi.dao.RuanganDao;
import id.ac.tazkia.matrikulasi.entity.PenjadwalanKegiatanTemporer;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class PenjadwalanKegiatanTemporerController {

    @Autowired private PenjadwalanKegiatanTemporerDao temporerDao;
    @Autowired private RuanganDao ruanganDao;
    @Autowired private KegiatanDao kegiatanDao;

    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    @GetMapping("/aktifitas/kegiatan_temporer")
    public String kegiatanTemporer(@RequestParam(required = false) String search ,
                                   ModelMap modelMap, @PageableDefault(size = 20) Pageable pageable) {
        if (StringUtils.hasText(search)) {
            modelMap.addAttribute("listKegiatanTemporer", temporerDao.findByStatusOrderByTanggalKegiatan(StatusRecord.AKTIF, pageable, search));
        } else {
            modelMap.addAttribute("listKegiatanTemporer", temporerDao.findByStatus(StatusRecord.AKTIF, pageable));
        }
        return "aktifitas/kegiatan_temporer/list";
    }

    @GetMapping("/aktifitas/kegiatan_temporer/new")
    public String kegiatanTemporerNew(@RequestParam(required = false) String id,
                                      ModelMap modelMap, Pageable pageable) {
        if (StringUtils.hasText(id)) {
            modelMap.addAttribute("kegiatanTemporer", temporerDao.findById(id).get());
        } else {
            modelMap.addAttribute("kegiatanTemporer", new PenjadwalanKegiatanTemporer());
        }
        modelMap.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
        modelMap.addAttribute("kegiatan", kegiatanDao.findByStatusOrderByNamaKegiatanAsc(StatusRecord.AKTIF, pageable));

        return "aktifitas/kegiatan_temporer/form";
    }

    @PostMapping("/aktifitas/kegiatan_temporer/new")
    public String kegiatanTemporerForm(@ModelAttribute @Valid PenjadwalanKegiatanTemporer temporer,
                                       Pageable pageable, BindingResult errors,
                                       ModelMap modelMap, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            modelMap.addAttribute("kegiatanTemporer", temporer);
            modelMap.addAttribute("kegiatan", kegiatanDao.findByStatusOrderByNamaKegiatanAsc(StatusRecord.AKTIF, pageable));
            modelMap.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
            return "aktifitas/kegiatan_temporer/form";
        }

        PenjadwalanKegiatanTemporer dupeTemporer2 = temporerDao.findByJamMulai(temporer.getJamMulai());
        if (dupeTemporer2 != null && !dupeTemporer2.getId().equalsIgnoreCase(temporer.getId())) {
            modelMap.addAttribute("error", "Jam mulai Duplicate");
            modelMap.addAttribute("kegiatanTemporer", temporer);
            modelMap.addAttribute("kegiatan", kegiatanDao.findByStatusOrderByNamaKegiatanAsc(StatusRecord.AKTIF, pageable));
            modelMap.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
            return "aktifitas/kegiatan_temporer/form";
        }

        PenjadwalanKegiatanTemporer dupeTemporer3 = temporerDao.findByStatusAndTanggalKegiatanString(StatusRecord.AKTIF, temporer.getTanggalKegiatanString());
        if (dupeTemporer3 != null && !dupeTemporer3.getId().equalsIgnoreCase(temporer.getId())) {
            modelMap.addAttribute("error", "Tanggal Kegiatan Duplicate");
            modelMap.addAttribute("kegiatanTemporer", temporer);
            modelMap.addAttribute("kegiatan", kegiatanDao.findByStatusOrderByNamaKegiatanAsc(StatusRecord.AKTIF, pageable));
            modelMap.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
            return "aktifitas/kegiatan_temporer/form";
        }

        String date = temporer.getTanggalKegiatanString();
        String tahun = date.substring(6, 10);
        String bulan = date.substring(0, 2);
        String tanggal = date.substring(3, 5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        temporer.setTanggalKegiatan(localDate.plusDays(0));

        try {
            temporerDao.save(temporer);
            attributes.addFlashAttribute("success", "Save Data Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/aktifitas/kegiatan_temporer";
    }

    @PostMapping("/aktifitas/kegiatan_temporer/hapus")
    public String kegiatanTemporerDelete(@RequestParam(required = false) String id,
                                         RedirectAttributes attributes) {

        PenjadwalanKegiatanTemporer temporers = temporerDao.findById(id).get();
        temporers.setStatus(StatusRecord.HAPUS);
        temporerDao.save(temporers);

        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/aktifitas/kegiatan_temporer";
    }

}