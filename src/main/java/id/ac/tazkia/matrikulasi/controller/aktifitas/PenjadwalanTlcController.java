package id.ac.tazkia.matrikulasi.controller.aktifitas;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PenjadwalanTlcController {

    @GetMapping("aktifitas/penjadwalan_tlc")
    private String penjadwalanTlc() {
        return "aktifitas/penjadwalan_tlc/list";
    }

    @GetMapping("aktifitas/penjadwalan_tlc/new")
    private String penjadwalanTlcNew() {
        return "aktifitas/penjadwalan_tlc/form";
    }
}
