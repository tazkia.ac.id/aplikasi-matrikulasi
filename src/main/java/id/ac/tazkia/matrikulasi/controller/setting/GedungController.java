package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.GedungDao;
import id.ac.tazkia.matrikulasi.dao.MatrikulasiDao;
import id.ac.tazkia.matrikulasi.entity.Gedung;
import id.ac.tazkia.matrikulasi.entity.Matrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
//import jdk.net.SocketFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class GedungController {

//    private static final String TEMPLATE_LIST = "member/complaint/list";
//    private static final String TEMPLATE_VIEW = "member/complaint/view";
//    private static final String TEMPLATE_FORM = "setting/gedung/form";

    private @Autowired GedungDao gedungDao;
    private @Autowired MatrikulasiDao matrikulasiDao;

    @GetMapping("/setting/gedung")
    public String showGedung(Gedung gedung, ModelMap modelMap, @PageableDefault Pageable pageable){
        modelMap.addAttribute("datas", gedungDao.findByStatusOrderByNamaGedungDesc(StatusRecord.AKTIF,pageable));
        modelMap.addAttribute("gedung", gedung);

        return "setting/setting_gedung/list";
    }

    @GetMapping("/setting/gedung/new")
    public String gedungNew(@RequestParam(required = false)String id, ModelMap mm){

        if (StringUtils.hasText(id)) {
            mm.addAttribute("gedung", gedungDao.findById(id).get());
        } else {
            mm.addAttribute("gedung", new Gedung());
        }

        return "setting/setting_gedung/form";
    }

    @PostMapping("/setting/gedung/new")
    public String prosesForm(@ModelAttribute @Valid Gedung gedung, BindingResult errors, ModelMap mm,
                             RedirectAttributes attributes) {

        if (errors.hasErrors()) {
            mm.addAttribute("gedung", gedung);
            return "setting/setting_gedung/form";
        }

//        try {
            Matrikulasi matrikulasi = matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1");
            gedung.setMatrikulasi(matrikulasi);
            gedungDao.save(gedung);
            attributes.addFlashAttribute("success", "Save Data Berhasil");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return "redirect:/setting/gedung";
    }

    @PostMapping("/setting/gedung/delete")
    public String deleteStatus(@RequestParam(value = "id", required = false) String id, Gedung gedung) {
        try {

            gedung = gedungDao.findById(id).get();
            gedung.setStatus(StatusRecord.HAPUS);
            gedungDao.save(gedung);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/setting/gedung";
    }


}
