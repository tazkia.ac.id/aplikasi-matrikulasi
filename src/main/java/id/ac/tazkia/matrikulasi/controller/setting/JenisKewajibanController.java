package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.JenisKewajibanDao;
import id.ac.tazkia.matrikulasi.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class JenisKewajibanController {

    @Autowired private JenisKewajibanDao jenisKewajibanDao;

    @GetMapping("setting/jenis_kewajiban")
    private String jenisKewajiban(ModelMap model, @PageableDefault (size = 10) Pageable page) {
        model.addAttribute("datas", jenisKewajibanDao.findByStatus(StatusRecord.AKTIF, page));
        return "setting/jenis_kewajiban/list";
    }

    @GetMapping("setting/jenis_kewajiban/new")
    private String newJenisKewajiban(@RequestParam(required = false) String id, ModelMap modelMap) {
        if (StringUtils.hasText(id)) {
            modelMap.addAttribute("jenisKewajiban", jenisKewajibanDao.findById(id).get());
        } else {
            modelMap.addAttribute("jenisKewajiban", new JenisKewajiban());
        }
        return "setting/jenis_kewajiban/form";
    }

    @PostMapping("setting/jenis_kewajiban/new")
    public String formjenisKewajiban(@Valid JenisKewajiban jenisKewajiban, BindingResult bindingResult,
                            ModelMap modelMap, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("jenisKewajiban", jenisKewajiban);
            return "setting/jenis_kewajiban/form";
        } try {
            jenisKewajibanDao.save(jenisKewajiban);
            redirectAttributes.addFlashAttribute("success", "Data saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/setting/jenis_kewajiban";
    }

    @PostMapping("setting/jenis_kewajiban/delete")
    public String deleteFormJenisKewajiban(@RequestParam (required = false) String kewajiban, RedirectAttributes attributes) {

        JenisKewajiban jenisKewajiban = jenisKewajibanDao.findById(kewajiban).get();

        jenisKewajiban.setStatus(StatusRecord.HAPUS);
        jenisKewajibanDao.save(jenisKewajiban);

        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/jenis_kewajiban";
    }
}