package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.JenisRuangDao;
import id.ac.tazkia.matrikulasi.entity.JenisRuang;
import id.ac.tazkia.matrikulasi.entity.PejabatMatrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class JenisRuangController {

    @Autowired private JenisRuangDao jenisRuangDao;

    @GetMapping("setting/jenis_ruang")
    private String jenisRuang(ModelMap modelMap, @PageableDefault Pageable pageable) {
        modelMap.addAttribute("datas",jenisRuangDao.findByStatus(StatusRecord.AKTIF, pageable));
        return "setting/jenis_ruang/list";
    }

    @GetMapping("setting/jenis_ruang/new")
    private String jenisRuangNew(@RequestParam(required = false) String id, ModelMap modelMap) {
        if (StringUtils.hasText(id)) {
            modelMap.addAttribute("jenisRuang", jenisRuangDao.findById(id).get());
        } else {
            modelMap.addAttribute("jenisRuang", new JenisRuang());
        }
        return "setting/jenis_ruang/form";
    }

    @PostMapping("setting/jenis_ruang/new")
    public String inputForm(@Valid JenisRuang jenisRuang, BindingResult bindingResult,
                            ModelMap modelMap, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("jenisRuang", jenisRuang);
            return "setting/jenis_ruang/form";
        } try {
            jenisRuangDao.save(jenisRuang);
            redirectAttributes.addFlashAttribute("success", "Data saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/setting/jenis_ruang";
    }

    @PostMapping("setting/jenis_ruang/delete")
    public String deleteForm(@RequestParam (required = false) String jenisRuang, RedirectAttributes attributes) {

        JenisRuang jenisRuangs = jenisRuangDao.findById(jenisRuang).get();
        jenisRuangs.setStatus(StatusRecord.HAPUS);
        jenisRuangDao.save(jenisRuangs);
        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/jenis_ruang";
    }
}