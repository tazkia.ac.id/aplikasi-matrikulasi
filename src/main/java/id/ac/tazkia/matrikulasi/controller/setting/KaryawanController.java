package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.AgamaDao;
import id.ac.tazkia.matrikulasi.dao.KaryawanDao;
import id.ac.tazkia.matrikulasi.dao.LembagaDao;
import id.ac.tazkia.matrikulasi.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Controller
public class KaryawanController {

    private static final Logger LOGGER = LoggerFactory.getLogger(KaryawanController.class);

    private @Autowired KaryawanDao karyawanDao;
    private @Autowired AgamaDao agamaDao;
    private @Autowired LembagaDao lembagaDao;

    @Autowired
    @Value("${upload.file}")
    private String uploadFolder;

    @GetMapping("/setting/employee")
    public String showEmployee(@RequestParam(required = false)String search, ModelMap mm, @PageableDefault(size = 10) Pageable page){

        if (StringUtils.hasText(search)) {
            mm.addAttribute("karyawan", karyawanDao.findByStatusAndSearch(search, StatusRecord.AKTIF, page));
        } else {
            mm.addAttribute("karyawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, page));
        }

        return "setting/setting_employee/list";
    }

    @GetMapping("/setting/employee/new")
    public String employeeNew(@RequestParam(required = false)String id, ModelMap mm){


        if (StringUtils.hasText(id)) {
            mm.addAttribute("karyawan", karyawanDao.findById(id).get());
        } else {
            mm.addAttribute("karyawan", new Karyawan());
        }

        mm.addAttribute("agama", agamaDao.findByStatusOrderByAgamaAsc(StatusRecord.AKTIF));
        mm.addAttribute("listLembaga", lembagaDao.findByStatusOrderByNamaLembagaDesc(StatusRecord.AKTIF));

        return "setting/setting_employee/form";
    }

    @PostMapping("/setting/employee/new")
    public String prosesEmployee(@ModelAttribute @Valid Karyawan karyawan, ModelMap mm,
                                 BindingResult errors, RedirectAttributes attributes,
                                 @RequestParam("lampiran") MultipartFile file) {

        if (errors.hasErrors()) {
            mm.addAttribute("karyawan", karyawan);
            return "setting/setting_employee/form";
        }

        try {

            String namaFile =  file.getName();
            String jenisFile = file.getContentType();
            String namaAsli = file.getOriginalFilename();
            Long ukuran = file.getSize();

            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }

            String idFoto = UUID.randomUUID().toString();

            if (ukuran == 0) {
                karyawan.setFileFoto("default.jpg");
            } else {
                karyawan.setFileFoto(idFoto + "." + extension);
            }

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);


            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFoto + "." + extension);
            file.transferTo(tujuan);

            String date = karyawan.getTanggalLahirString();
            String tahun = date.substring(6,10);
            String bulan = date.substring(0,2);
            String tanggal = date.substring(3,5);
            String tanggalan = tahun + '-' + bulan + '-' + tanggal;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalan, formatter);
            karyawan.setTanggalLahir(localDate.plusDays(0));

            Lembaga lembaga = lembagaDao.findByStatusAndId(StatusRecord.AKTIF, "1");
            karyawan.setLembaga(lembaga);
            karyawanDao.save(karyawan);
            attributes.addFlashAttribute("success", "Save Data Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/setting/employee";
    }


    @PostMapping("setting/employee/hapus")
    public String deleteEmployee(@RequestParam(required = false)String id,
                                           RedirectAttributes attributes){

        Karyawan karyawan = karyawanDao.findById(id).get();

        karyawan.setStatus(StatusRecord.HAPUS);
        karyawanDao.save(karyawan);

        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/employee";

    }

}
