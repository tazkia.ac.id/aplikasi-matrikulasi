package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.KegiatanDao;
import id.ac.tazkia.matrikulasi.dao.MatrikulasiDao;
import id.ac.tazkia.matrikulasi.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class KegiatanController {

    @Autowired private KegiatanDao kegiatanDao;
    @Autowired private MatrikulasiDao matrikulasiDao;

    @GetMapping("setting/nama_kegiatan")
    private String kegiatanList(Kegiatan kegiatan, ModelMap modelMap, @PageableDefault Pageable pageable) {
        modelMap.addAttribute("kegiatan", kegiatan);
        modelMap.addAttribute("datas", kegiatanDao.findByStatusOrderByNamaKegiatanAsc((StatusRecord.AKTIF), pageable));
        return "setting/nama_kegiatan/list";
    }

    @GetMapping("setting/nama_kegiatan/new")
    private String kegiatanNew(@RequestParam(required = false) String id, ModelMap modelMap) {
        if (StringUtils.hasText(id)) {
            modelMap.addAttribute("kegiatan", kegiatanDao.findById(id).get());
            modelMap.addAttribute("matrikulasi", matrikulasiDao.findAll());
        } else {
            modelMap.addAttribute("kegiatan", new Kegiatan());
        }
        return "setting/nama_kegiatan/form";
    }

    @PostMapping("setting/nama_kegiatan/new")
    public String inputForm(@Valid Kegiatan kegiatan, BindingResult bindingResult,
                            ModelMap modelMap, RedirectAttributes attributes) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("kegiatan", kegiatan);
            return "setting/nama_kegiatan/form";
        }

        Matrikulasi matrikulasi = matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1");
        kegiatan.setMatrikulasi(matrikulasi);
        kegiatanDao.save(kegiatan);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:/setting/nama_kegiatan";
    }

    @PostMapping("setting/nama_kegiatan/hapus")
    public String deleteForm(@RequestParam(value = "id", required = false) String id,
                             Kegiatan kegiatan, RedirectAttributes attributes) {
        try {
            kegiatan = kegiatanDao.findById(id).get();
            kegiatan.setStatus(StatusRecord.HAPUS);
            kegiatanDao.save(kegiatan);
        } catch (Exception e) {
            e.printStackTrace();
        }
        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/nama_kegiatan";
    }
}
