package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.JenisKewajibanDao;
import id.ac.tazkia.matrikulasi.dao.KewajibanDao;
import id.ac.tazkia.matrikulasi.dao.MatrikulasiDao;
import id.ac.tazkia.matrikulasi.dao.ProdiDao;
import id.ac.tazkia.matrikulasi.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class KewajibanController {

    @Autowired private KewajibanDao kewajibanDao;
    @Autowired private MatrikulasiDao matrikulasiDao;
    @Autowired private JenisKewajibanDao jenisKewajibanDao;

    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    @GetMapping("setting/setting_kewajiban")
    public String kewajiban(Model model, @PageableDefault(size = 10) Pageable page) {
        model.addAttribute("listKewajiban", kewajibanDao.findByStatus(StatusRecord.AKTIF, page));
        return "setting/setting_kewajiban/list";
    }

    @GetMapping("setting/setting_kewajiban/new")
    public String kewajibanNew(Model model, @PageableDefault Pageable pageable) {
        model.addAttribute("jenisKewajiban", jenisKewajibanDao.findByStatusOrderByNamaJenisKewajibanAsc(StatusRecord.AKTIF, pageable));
        model.addAttribute("kewajiban", new Kewajiban());
        return "setting/setting_kewajiban/form";

    }

    @GetMapping("setting/setting_kewajiban/edit")
    public String kewajibanEdit(Model model, @RequestParam(required = false) String kewajiban, Pageable page) {
        model.addAttribute("jenisKewajiban", jenisKewajibanDao.findByStatusOrderByNamaJenisKewajibanAsc(StatusRecord.AKTIF, page));
        model.addAttribute("kewajiban", kewajibanDao.findById(kewajiban));
        return "setting/setting_kewajiban/form";

    }

    @PostMapping("setting/setting_kewajiban")
    public String savePejabatMatrikulasi(@ModelAttribute @Valid Kewajiban kewajiban,
                                         ModelMap modelMap, RedirectAttributes attributes, BindingResult errors) {

        String date = kewajiban.getTanggalMulaiBerlakuString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        kewajiban.setTanggalMulaiBerlaku(localDate.plusDays(0));

        Matrikulasi matrikulasi = matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1");
        kewajiban.setMatrikulasi(matrikulasi);
        kewajibanDao.save(kewajiban);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:/setting/setting_kewajiban";

    }

    @PostMapping("setting/setting_kewajiban/hapus")
    public String deletePejabatMatrikulasi(@RequestParam(required = false)String wajib,
                                           RedirectAttributes attributes){

        Kewajiban kewajiban = kewajibanDao.findById(wajib).get();
        kewajiban.setStatus(StatusRecord.HAPUS);
        kewajibanDao.save(kewajiban);
        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/setting_kewajiban";

    }
}
