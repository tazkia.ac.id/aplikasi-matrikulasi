package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.KampusDao;
import id.ac.tazkia.matrikulasi.dao.MatrikulasiDao;
import id.ac.tazkia.matrikulasi.entity.Kampus;
import id.ac.tazkia.matrikulasi.entity.Matrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
//import javafx.scene.control.TableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class OurMatrikulasiController {

//    private static final String TEMPLATE_LIST = "member/complaint/list";
//    private static final String TEMPLATE_VIEW = "member/complaint/view";
    private static final String TEMPLATE_FORM = "setting/our_matrikulasi/form";

    private @Autowired MatrikulasiDao matrikulasiDao;

    @Autowired private KampusDao kampusDao;

    @GetMapping("/setting/our_matrikulasi")
    public String ourMatrikulasi(@RequestParam(required = false)String id, ModelMap modelMap){

//        if (StringUtils.hasText(id)) {
//            modelMap.addAttribute("matrikulasi", matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1"));
//        } else {
//            modelMap.addAttribute("matrikulasi", matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1"));
//        }

        Matrikulasi matrikulasi = matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1");

        if (matrikulasi == null){
            modelMap.addAttribute("matrikulasi", new Matrikulasi());
        }else{
            modelMap.addAttribute("matrikulasi", matrikulasi);
        }


        return "setting/our_matrikulasi/form";
    }

    @PostMapping("/setting/our_matrikulasi")
    public String prosesForm(@Valid Matrikulasi matrikulasi, BindingResult errors, ModelMap modelMap) {

        if (errors.hasErrors()) {
            modelMap.addAttribute("matrikulasi", matrikulasi);
            return "setting/our_matrikulasi/form";
        }

        try {
            Kampus kampus = kampusDao.findByStatusAndId(StatusRecord.AKTIF, "1");
            matrikulasi.setKampus(kampus);
            matrikulasiDao.save(matrikulasi);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/setting/our_matrikulasi";
    }
}
