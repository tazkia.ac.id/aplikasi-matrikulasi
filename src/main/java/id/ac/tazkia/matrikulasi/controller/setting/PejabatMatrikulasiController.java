package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.KaryawanDao;
import id.ac.tazkia.matrikulasi.dao.LembagaDao;
import id.ac.tazkia.matrikulasi.dao.MatrikulasiDao;
import id.ac.tazkia.matrikulasi.dao.PejabatMatrikulasiDao;
import id.ac.tazkia.matrikulasi.entity.Matrikulasi;
import id.ac.tazkia.matrikulasi.entity.PejabatMatrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class PejabatMatrikulasiController {

    @Autowired
    private LembagaDao lembagaDao;
    @Autowired
    private MatrikulasiDao matrikulasiDao;
    @Autowired
    private PejabatMatrikulasiDao pejabatMatrikulasiDao;
    @Autowired
    private KaryawanDao karyawanDao;

        @GetMapping("setting/pejabat_matrikulasi")
        public String matriculationPejabat(Model model, @PageableDefault(size = 10) Pageable page){
            model.addAttribute("listPejabatMatrikulasi", pejabatMatrikulasiDao.findByStatusOrderByTanggalMulaiDesc(StatusRecord.AKTIF, page));
            return "setting/pejabat_matrikulasi/list";
        }

        @GetMapping("/setting/pejabat_matrikulasi/new")
        public String matriculationPejabatNew (Model model, @PageableDefault Pageable page){

            model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, page));
            model.addAttribute("pejabatMatrikulasi", new PejabatMatrikulasi());

            return "setting/pejabat_matrikulasi/form";

        }

        @GetMapping("setting/pejabat_matrikulasi/edit")
        public String matriculationPejabatEdit (Model model, @RequestParam(required = false) String pejabat, Pageable pageable){

            model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF, pageable));
            model.addAttribute("pejabatMatrikulasi", pejabatMatrikulasiDao.findById(pejabat));

            return "setting/pejabat_matrikulasi/form";

        }

        @PostMapping("/setting/pejabat_matrikulasi")
        public String savePejabatMatrikulasi (@ModelAttribute @Valid PejabatMatrikulasi pejabatMatrikulasi,
                RedirectAttributes attributes,
                BindingResult errors){

            String date = pejabatMatrikulasi.getTanggalMulaiString();
            String tahun = date.substring(6, 10);
            String bulan = date.substring(0, 2);
            String tanggal = date.substring(3, 5);
            String tanggalan = tahun + '-' + bulan + '-' + tanggal;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalan, formatter);
            pejabatMatrikulasi.setTanggalMulai(localDate.plusDays(0));

            String date1 = pejabatMatrikulasi.getTanggalSelesaiString();
            String tahun1 = date1.substring(6, 10);
            String bulan1 = date1.substring(0, 2);
            String tanggal1 = date1.substring(3, 5);
            String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);
            pejabatMatrikulasi.setTanggalSelesai(localDate1.plusDays(0));

            Matrikulasi matrikulasi = matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1");
            pejabatMatrikulasi.setMatrikulasi(matrikulasi);
            pejabatMatrikulasiDao.save(pejabatMatrikulasi);

            attributes.addFlashAttribute("success", "Save Data Berhasil");
            return "redirect:/setting/pejabat_matrikulasi";

        }

        @PostMapping("setting/pejabat_matrikulasi/hapus")
        public String deletePejabatMatrikulasi (@RequestParam(required = false) String pejabat,
                RedirectAttributes attributes){

            PejabatMatrikulasi pejabatMatrikulasi = pejabatMatrikulasiDao.findById(pejabat).get();

            pejabatMatrikulasi.setStatus(StatusRecord.HAPUS);
            pejabatMatrikulasiDao.save(pejabatMatrikulasi);

            attributes.addFlashAttribute("deleted", "Save Data Berhasil");
            return "redirect:/setting/pejabat_matrikulasi";

        }
    }