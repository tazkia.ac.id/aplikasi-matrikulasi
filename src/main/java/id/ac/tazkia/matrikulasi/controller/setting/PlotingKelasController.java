package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.KelasDao;
import id.ac.tazkia.matrikulasi.dao.MahasiswaDao;
import id.ac.tazkia.matrikulasi.dao.PlotingKelasDao;
import id.ac.tazkia.matrikulasi.entity.JenisKelamin;
import id.ac.tazkia.matrikulasi.entity.Kelas;
import id.ac.tazkia.matrikulasi.entity.PlotingKelas;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PlotingKelasController {

    @Autowired
    private MahasiswaDao mahasiswaDao;
    @Autowired
    private PlotingKelasDao plotingKelasDao;
    @Autowired
    private KelasDao kelasDao;

    @GetMapping("/setting/ploting_kelas")
    public String plotingKelas(ModelMap model,
                               @PageableDefault Pageable pageable,
                               @RequestParam(required = false) String jenisKelamin,
                               @RequestParam(required = false) String angkatan,
                               @RequestParam(required = false) String search) {

        if (jenisKelamin != null & angkatan != null) {
            if (jenisKelamin.equalsIgnoreCase("GABUNGAN")) {
                model.addAttribute("listPlotingKelas", plotingKelasDao.listPlotingKelasTanpaKenisKelamin(angkatan));
            } else {
                model.addAttribute("listPlotingKelas", plotingKelasDao.listPlotingKelas(angkatan, jenisKelamin));
            }
            model.addAttribute("selectedAngkatan", angkatan);
            model.addAttribute("selectedJenisKelamin", jenisKelamin);
            model.addAttribute("listMahasiswaPloting", plotingKelasDao.listPlotingMahasiswa(angkatan, jenisKelamin));
        }
        model.addAttribute("angkatan", mahasiswaDao.listAngkatan());
        model.addAttribute("jenisKelamin", kelasDao.listJenisKelamin());


        return "setting/ploting_kelas/list";
    }

    @GetMapping("/setting/ploting_kelas/new")
    public String KelasNew(Kelas kelas,
                           @RequestParam(required = false) String angkatan,
                           @RequestParam(required = false) String jenisKelamin, ModelMap model) {

        model.addAttribute("kelas", kelas);
        model.addAttribute("selectedJenisKelamin", jenisKelamin);
        model.addAttribute("angkatan", mahasiswaDao.listAngkatan());
        model.addAttribute("jenisKelamin", kelasDao.listJenisKelamin());

        return "setting/ploting_kelas/form";
    }

    @PostMapping("/setting/ploting_kelas/new")
    public String kelasNewProcess(@Valid Kelas kelas, ModelMap model,
                                  BindingResult errors, RedirectAttributes attributes) {

        if (errors.hasErrors()) {
            model.addAttribute("kelas", kelas);
        }

        kelasDao.save(kelas);
        attributes.addFlashAttribute("success", "Save Data Berhasil");

        return "redirect:/setting/ploting_kelas";
    }

    @GetMapping("/setting/ploting_kelas/proses")
    public String prosesPlotingKelas(ModelMap model,
                                     @RequestParam(required = false) String kelas,
                                     @RequestParam(required = false) String search,
                                     @RequestParam(required = false) String jenisKelamin,
                                     @RequestParam(required = false) List<String> mahasiswas,
                                     @RequestParam(required = false) String angkatan) {

        if (mahasiswas != null) {
            for (String idM : mahasiswas) {
                List<PlotingKelas> plotingKelases = plotingKelasDao.findByStatusAndMahasiswaId(StatusRecord.AKTIF, idM);
                for (PlotingKelas p : plotingKelases) {
                    p.setStatus(StatusRecord.HAPUS);
                    plotingKelasDao.save(p);
                }

                PlotingKelas plotingKelas = new PlotingKelas();
                plotingKelas.setMahasiswa(mahasiswaDao.findById(idM).get());
                plotingKelas.setKelas(kelasDao.findById(kelas).get());
                plotingKelasDao.save(plotingKelas);
            }
        }

        return "redirect:../ploting_kelas?angkatan=" + angkatan + "&jenisKelamin=" + jenisKelamin + "&search=" + search;
    }
}