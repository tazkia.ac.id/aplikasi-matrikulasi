package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.KaryawanDao;
import id.ac.tazkia.matrikulasi.dao.KelasDao;
import id.ac.tazkia.matrikulasi.dao.MahasiswaDao;
import id.ac.tazkia.matrikulasi.dao.PlotingPembinaDao;
import id.ac.tazkia.matrikulasi.entity.JenisKelamin;
import id.ac.tazkia.matrikulasi.entity.Mahasiswa;
import id.ac.tazkia.matrikulasi.entity.PlotingPembina;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.Access;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class PlotingPembinaController {

    @Autowired private KaryawanDao karyawanDao;
    @Autowired private KelasDao kelasDao;
    @Autowired private MahasiswaDao mahasiswaDao;
    @Autowired private PlotingPembinaDao plotingPembinaDao;

    @GetMapping("setting/ploting_pembina")
    public String plotingPembina(Model model,
                                 @RequestParam(required = false) String jenisKelamin,
                                 @RequestParam(required = false) String angkatan,
                                 @RequestParam(required = false) String search){

        if (jenisKelamin != null & angkatan != null){

            model.addAttribute("selectedAngkatan", angkatan);
            model.addAttribute("selectedJenisKelamin", jenisKelamin);
            model.addAttribute("angkatan", mahasiswaDao.listAngkatan());
            model.addAttribute("jenisKelamin", kelasDao.listJenisKelamin());
            model.addAttribute("listPlotingPembina", plotingPembinaDao.listPlotingPembina(angkatan, jenisKelamin));
            model.addAttribute("listMahasiswaPloting", plotingPembinaDao.listPlotingMahasiswa(angkatan,jenisKelamin));

        }else{

            model.addAttribute("angkatan", mahasiswaDao.listAngkatan());
            model.addAttribute("jenisKelamin", kelasDao.listJenisKelamin());

        }



        return "setting/ploting_pembina/list";
    }


    @GetMapping("setting/ploting_pembina/proses")
    public String prosesPlotingPembina(Model model,
                                       @RequestParam(required = false) String pembina,
                                       @RequestParam(required = false) List<String> mahasiswas,
                                       @RequestParam(required = false) String angkatan,
                                       @RequestParam(required = false) String jenisKelamin,
                                       @RequestParam(required = false) String search){

        if(mahasiswas != null){

            for (String a : mahasiswas){

                List<PlotingPembina> plotingPembinas = plotingPembinaDao.findByStatusAndMahasiswaId(StatusRecord.AKTIF, a);
                for (PlotingPembina b : plotingPembinas){
                    b.setStatus(StatusRecord.HAPUS);
                    plotingPembinaDao.save(b);
                }

                PlotingPembina plotingPembina= new PlotingPembina();
                plotingPembina.setMahasiswa(mahasiswaDao.findById(a).get());
                plotingPembina.setPembina(karyawanDao.findById(pembina).get());
                plotingPembina.setWaktuPloting(LocalDateTime.now());
                plotingPembinaDao.save(plotingPembina);

            }

        }


        return "redirect:../ploting_pembina?angkatan="+ angkatan +"&jenisKelamin=" + jenisKelamin +"&search="+ search;
    }

}
