package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.KelasTlcDao;
import id.ac.tazkia.matrikulasi.dao.MahasiswaDao;
import id.ac.tazkia.matrikulasi.dao.PlotingTlcDao;
import id.ac.tazkia.matrikulasi.entity.KelasTlc;
import id.ac.tazkia.matrikulasi.entity.PlotingTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PlotingTlcController {

    @Autowired
    private MahasiswaDao mahasiswaDao;

    @Autowired
    private PlotingTlcDao plotingTlcDao;

    @Autowired
    private KelasTlcDao kelasTlcDao;

    @GetMapping("/setting/ploting_tlc")
    public String plotingTlc(ModelMap model,
                               @PageableDefault Pageable pageable,
                               @RequestParam(required = false) String jenisKelamin,
                               @RequestParam(required = false) String angkatan,
                               @RequestParam(required = false) String search) {

        if (jenisKelamin != null & angkatan != null) {
            if (jenisKelamin.equalsIgnoreCase("GABUNGAN")) {
                model.addAttribute("listPlotingTlc", plotingTlcDao.listPlotingTlcTanpaJenisKelamin(angkatan));
            } else {
                model.addAttribute("listPlotingTlc", plotingTlcDao.listPlotingTlc(angkatan, jenisKelamin));
            }
            model.addAttribute("selectedAngkatan", angkatan);
            model.addAttribute("jenisKelamin", jenisKelamin);
            model.addAttribute("listMahasiswaPloting", plotingTlcDao.listPlotingMahasiswa(angkatan, jenisKelamin));
        }
        model.addAttribute("angkatan", mahasiswaDao.listAngkatan());
        model.addAttribute("jenisKelamin", kelasTlcDao.listJenisKelamin());

        return "setting/ploting_tlc/list";
    }

    @GetMapping("/setting/ploting_tlc/new")
    public String KelasNew(KelasTlc kelasTlc, ModelMap model,
                           @RequestParam(required = false) String angkatan,
                           @RequestParam(required = false) String jenisKelamin) {

        model.addAttribute("kelasTlc", kelasTlc);
        model.addAttribute("selectedJenisKelamin", jenisKelamin);
        model.addAttribute("angkatan", mahasiswaDao.listAngkatan());
        model.addAttribute("jenisKelamin", kelasTlcDao.listJenisKelamin());

        return "setting/ploting_tlc/form";
    }

    @PostMapping("/setting/ploting_tlc/new")
    public String tlcNewProcess(@Valid KelasTlc kelasTlc, ModelMap model,
                                  BindingResult errors, RedirectAttributes attributes) {

        if (errors.hasErrors()) {
            model.addAttribute("kelasTlc", kelasTlc);
        }
        kelasTlcDao.save(kelasTlc);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:/setting/ploting_tlc";
    }

    @GetMapping("/setting/ploting_tlc/proses")
    public String prosesPlotingTlc(ModelMap model,
                                     @RequestParam(required = false) String kelasTlc,
                                     @RequestParam(required = false) String search,
                                     @RequestParam(required = false) String jenisKelamin,
                                     @RequestParam(required = false) List<String> mahasiswa,
                                     @RequestParam(required = false) String angkatan) {

        if (mahasiswa != null) {
            for (String id : mahasiswa) {
                List<PlotingTlc> plotingTlcs = plotingTlcDao.findByStatusAndMahasiswaId(StatusRecord.AKTIF, id);
                for (PlotingTlc p : plotingTlcs) {
                    p.setStatus(StatusRecord.HAPUS);
                    plotingTlcDao.save(p);
                }

                PlotingTlc plotingTlc = new PlotingTlc();
                plotingTlc.setMahasiswa(mahasiswaDao.findById(id).get());
                plotingTlc.setKelasTlc(kelasTlcDao.findById(kelasTlc).get());
                plotingTlcDao.save(plotingTlc);
            }
        }

        return "redirect:../ploting_tlc?angkatan="+ angkatan + "&jenisKelamin="+ jenisKelamin +"&search="+ search;
    }
}