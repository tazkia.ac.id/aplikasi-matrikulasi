package id.ac.tazkia.matrikulasi.controller.setting;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RoomController {

    @GetMapping("setting/kamar")
    public String room() {
        return "setting/kamar/list";
    }

    @GetMapping("setting/kamar/new")
    public String roomNew() {
        return "setting/kamar/form";
    }
}
