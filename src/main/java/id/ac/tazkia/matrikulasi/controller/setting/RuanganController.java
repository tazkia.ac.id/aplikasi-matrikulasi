package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.GedungDao;
import id.ac.tazkia.matrikulasi.dao.JenisRuangDao;
import id.ac.tazkia.matrikulasi.dao.RuanganDao;
import id.ac.tazkia.matrikulasi.entity.Gedung;
import id.ac.tazkia.matrikulasi.entity.JenisRuang;
import id.ac.tazkia.matrikulasi.entity.Ruangan;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class RuanganController {

    @Autowired private RuanganDao ruanganDao;
    @Autowired private GedungDao gedungDao;
    @Autowired private JenisRuangDao jenisRuangDao;

    @GetMapping("setting/ruangan")
    private String jenisRuang(Ruangan ruangan,ModelMap modelMap, @PageableDefault Pageable pageable) {
        modelMap.addAttribute("ruangan", ruangan);
        modelMap.addAttribute("datas", ruanganDao.findByStatus((StatusRecord.AKTIF), pageable));
        return "setting/ruangan/list";
    }

    @GetMapping("setting/ruangan/new")
    private String jenisRuangNew(@RequestParam(required = false) String id, ModelMap modelMap) {
        if (StringUtils.hasText(id)) {
            modelMap.addAttribute("ruangan", ruanganDao.findById(id).get());
            modelMap.addAttribute("jenisRuang", jenisRuangDao.findAll());
            modelMap.addAttribute("gedung", gedungDao.findAll());
        } else {
            modelMap.addAttribute("ruangan", new Ruangan());
        }
        modelMap.addAttribute("jenisRuang", jenisRuangDao.findAll());
        modelMap.addAttribute("gedung", gedungDao.findAll());

        return "setting/ruangan/form";
    }

    @PostMapping("setting/ruangan/new")
    public String inputForm(@Valid Ruangan ruangan, BindingResult bindingResult, ModelMap modelMap) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("jenisRuang", jenisRuangDao.findAll());
            modelMap.addAttribute("gedung", gedungDao.findAll());
            modelMap.addAttribute("ruangan", ruangan);
            return "setting/ruangan/form";
        }
        ruanganDao.save(ruangan);
        return "redirect:/setting/ruangan";
    }

    @PostMapping("setting/ruangan/delete")
    public String deleteForm(@RequestParam(value = "id", required = false) String id, Ruangan ruangan) {
        try {
            ruangan = ruanganDao.findById(id).get();
            ruangan.setStatus(StatusRecord.HAPUS);
            ruanganDao.save(ruangan);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/setting/ruangan";
    }
}