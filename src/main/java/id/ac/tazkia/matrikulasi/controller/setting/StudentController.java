package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.AgamaDao;
import id.ac.tazkia.matrikulasi.dao.LembagaDao;
import id.ac.tazkia.matrikulasi.dao.MahasiswaDao;
import id.ac.tazkia.matrikulasi.dao.ProdiDao;
import id.ac.tazkia.matrikulasi.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Controller
public class StudentController {

    @Autowired
    private AgamaDao agamaDao;

    @Autowired
    private LembagaDao lembagaDao;

    @Autowired
    private MahasiswaDao mahasiswaDao;

    @Autowired
    private ProdiDao prodiDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    @Value("${upload.file}")
    private String uploadFile;

    @GetMapping("/setting/student")
    public String student(ModelMap mm, @PageableDefault(size = 20) Pageable pageable) {
        mm.addAttribute("listMahasiswa", mahasiswaDao.listMahasiswa(pageable));
        return "setting/student/list";
    }

    @GetMapping("/setting/student/new")
    public String studentForm(@RequestParam(required = false) String id, ModelMap mm) {

        if (StringUtils.hasText(id)) {
            mm.addAttribute("mahasiswa", mahasiswaDao.findById(id).get());
        } else {
            mm.addAttribute("mahasiswa", new Mahasiswa());
        }
        mm.addAttribute("agama", agamaDao.findByStatusOrderByAgamaAsc(StatusRecord.AKTIF));
        mm.addAttribute("listLembaga", lembagaDao.findByStatusOrderByNamaLembagaDesc(StatusRecord.AKTIF));
        mm.addAttribute("listProdi", prodiDao.findByStatusOrderByKodeProdiAsc(StatusRecord.AKTIF));

        return "setting/student/form";
    }

    @GetMapping("/setting/student/edit")
    public String editStudent(ModelMap model, @RequestParam(required = false) Mahasiswa mahasiswa) {

        model.addAttribute("mahasiswa", mahasiswa);
        model.addAttribute("agama", agamaDao.findByStatusOrderByAgamaAsc(StatusRecord.AKTIF));
        model.addAttribute("listLembaga", lembagaDao.findByStatusOrderByNamaLembagaDesc(StatusRecord.AKTIF));
        model.addAttribute("listProdi", prodiDao.findByStatusOrderByKodeProdiAsc(StatusRecord.AKTIF));

        return "setting/student/form";
    }

    @PostMapping("/setting/student/new")
    public String prosesStudentForm(@ModelAttribute @Valid Mahasiswa mahasiswa,
                                    BindingResult errors, ModelMap mm,
                                    RedirectAttributes attributes,
                                    @RequestParam("lampiran") MultipartFile file) {

        if (errors.hasErrors()) {
            mm.addAttribute("mahasiswa", mahasiswa);
            mm.addAttribute("agama", agamaDao.findByStatusOrderByAgamaAsc(StatusRecord.AKTIF));
            mm.addAttribute("listLembaga", lembagaDao.findByStatusOrderByNamaLembagaDesc(StatusRecord.AKTIF));
            return "setting/student/form";
        }

        if (mahasiswa.getTanggalLahirString() == null) {
            mm.addAttribute("mahasiswa", mahasiswa);
            mm.addAttribute("agama", agamaDao.findByStatusOrderByAgamaAsc(StatusRecord.AKTIF));
            mm.addAttribute("listLembaga", lembagaDao.findByStatusOrderByNamaLembagaDesc(StatusRecord.AKTIF));
            mm.addAttribute("listProdi", prodiDao.findByStatusOrderByKodeProdiAsc(StatusRecord.AKTIF));
            mm.addAttribute("tanggalKosong", "Tanggal Lahir tidak boleh kosong");
            return "setting/student/form";
        }

        if (mahasiswa.getTanggalLahirString().isEmpty()) {
            mm.addAttribute("mahasiswa", mahasiswa);
            mm.addAttribute("agama", agamaDao.findByStatusOrderByAgamaAsc(StatusRecord.AKTIF));
            mm.addAttribute("listLembaga", lembagaDao.findByStatusOrderByNamaLembagaDesc(StatusRecord.AKTIF));
            mm.addAttribute("listProdi", prodiDao.findByStatusOrderByKodeProdiAsc(StatusRecord.AKTIF));
            mm.addAttribute("tanggalKosong", "Tanggal Lahir tidak boleh kosong");
            return "setting/student/form";
        }

        try {

            String namaFile = file.getName();
            String jenisFile = file.getContentType();
            String namaAsli = file.getOriginalFilename();
            Long ukuran = file.getSize();

            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }

            String idFoto = UUID.randomUUID().toString();

            if (ukuran == 0) {
                mahasiswa.setFileFoto("default.jpg");
            } else {
                mahasiswa.setFileFoto(idFoto + "." + extension);
            }

            System.out.println("file :" + namaAsli);
            System.out.println("Ukuran :" + ukuran);

            LOGGER.debug("Lokasi upload : {}", uploadFile);
            new File(uploadFile).mkdirs();
            File tujuan = new File(uploadFile + File.separator + idFoto + "." + extension);
            file.transferTo(tujuan);

            String date = mahasiswa.getTanggalLahirString();
            String tahun = date.substring(6, 10);
            String bulan = date.substring(0, 2);
            String tanggal = date.substring(3, 5);
            String tanggalan = tahun + '-' + bulan + '-' + tanggal;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalan, formatter);
            mahasiswa.setTanggalLahir(localDate.plusDays(0));

            Lembaga lembaga = lembagaDao.findByStatusAndId(StatusRecord.AKTIF, "1");
//            Prodi prodi = prodiDao.findByStatusAndId(StatusRecord.AKTIF, "1");
            mahasiswa.setLembaga(lembaga);
//            mahasiswa.setProdi(prodi);
            mahasiswaDao.save(mahasiswa);
            attributes.addFlashAttribute("success", "Save Data Berhasil");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/setting/student";
    }

    @PostMapping("/setting/student/hapus")
    public String deleteMahasiswa(@RequestParam(required = false) String mahasiswa,
                                  RedirectAttributes attributes) {

        Mahasiswa mahasiswas = mahasiswaDao.findById(mahasiswa).get();

        mahasiswas.setStatus(StatusRecord.HAPUS);
        mahasiswaDao.save(mahasiswas);

        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/student";

    }
}