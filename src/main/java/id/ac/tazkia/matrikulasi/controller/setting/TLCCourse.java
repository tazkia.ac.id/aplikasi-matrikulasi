package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.MatakuliahTlcDao;
import id.ac.tazkia.matrikulasi.entity.MatakuliahTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class TLCCourse {

    @Autowired
    private MatakuliahTlcDao matakuliahTlcDao;

    @GetMapping("setting/tlc_course")
    public String tlcCourse(Model model,
                            @RequestParam(required = false) String search,
                            @PageableDefault(size = 10) Pageable page) {

        model.addAttribute("listMatakuliahTlc", matakuliahTlcDao.findByStatusOrderByNamaMatakuliah(StatusRecord.AKTIF, page));

        return "setting/tlc_course/list";

    }

    @GetMapping("setting/tlc_course/new")
    public String tlcCourseNew(Model model) {

        model.addAttribute("matakuliahTlc", new MatakuliahTlc());

        return "setting/tlc_course/form";

    }

    @PostMapping("setting/tlc_course/save")
    public String saveTlcCourse(@Valid @ModelAttribute MatakuliahTlc matakuliahTlc,
                                RedirectAttributes attributes){


        matakuliahTlcDao.save(matakuliahTlc);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../tlc_course";

    }

}
