package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.MatrikulasiDao;
import id.ac.tazkia.matrikulasi.dao.TahunAkademikDao;
import id.ac.tazkia.matrikulasi.dao.TahunAkademikMatrikulasiDao;
import id.ac.tazkia.matrikulasi.entity.Matrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import id.ac.tazkia.matrikulasi.entity.TahunAkademikMatrikulasi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class TahunAkademikMatrikulasiController {

    @Autowired private TahunAkademikMatrikulasiDao tahunAkademikMatrikulasiDao;
    @Autowired private MatrikulasiDao matrikulasiDao;
    @Autowired private TahunAkademikDao tahunAkademikDao;

    @GetMapping("setting/tahun_akademik_matrikulasi")
    public String listTahunAkademikMatrikulasi(ModelMap modelMap, @PageableDefault Pageable pageable) {
        modelMap.addAttribute("tahunAkademikMatrikulasi", tahunAkademikMatrikulasiDao.findByStatusOrderByTanggalMulaiSemesterDesc(StatusRecord.AKTIF, pageable));
        return "setting/tahun_akademik_matrikulasi/list";
    }

    @GetMapping("setting/tahun_akademik_matrikulasi/new")
    public String newTahunAkademikMatrikulasi(@RequestParam(required = false) String tahunMatrikulasi, ModelMap modelMap) {

        if (StringUtils.hasText(tahunMatrikulasi)) {
            modelMap.addAttribute("listTahunAkademik", tahunAkademikDao.findByStatusOrderByKodeTahunAkademik(StatusRecord.AKTIF));
            modelMap.addAttribute("tahunAkademikMatrikulasi", tahunAkademikMatrikulasiDao.findById(tahunMatrikulasi).get());
        } else {
            modelMap.addAttribute("listTahunAkademik", tahunAkademikDao.findByStatusOrderByKodeTahunAkademik(StatusRecord.AKTIF));
            modelMap.addAttribute("tahunAkademikMatrikulasi", new TahunAkademikMatrikulasi());
        }

        return "setting/tahun_akademik_matrikulasi/form";
    }

    @PostMapping("/setting/tahun_akademik_matrikulasi/new")
    public String saveTahunAkademikMatrikulasi(@ModelAttribute @Valid TahunAkademikMatrikulasi tahunAkademikMatrikulasi,
                                         RedirectAttributes attributes,
                                         BindingResult errors, ModelMap model) {

        String date = tahunAkademikMatrikulasi.getTanggalMulaiSemesterString();
        String tahun = date.substring(6, 10);
        String bulan = date.substring(0, 2);
        String tanggal = date.substring(3, 5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        tahunAkademikMatrikulasi.setTanggalMulaiSemester(localDate.plusDays(0));

        String date1 = tahunAkademikMatrikulasi.getTanggalMulaiPenagihanString();
        String tahun1 = date1.substring(6, 10);
        String bulan1 = date1.substring(0, 2);
        String tanggal1 = date1.substring(3, 5);
        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);
        tahunAkademikMatrikulasi.setTanggalMulaiPenagihan(localDate1.plusDays(0));

        String date2 = tahunAkademikMatrikulasi.getTanggalSelesaiPenagihanString();
        String tahun2 = date2.substring(6, 10);
        String bulan2 = date2.substring(0, 2);
        String tanggal2 = date2.substring(3, 5);
        String tanggalan2 = tahun2 + '-' + bulan2 + '-' + tanggal2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalan2, formatter2);
        tahunAkademikMatrikulasi.setTanggalSelesaiPenagihan(localDate2.plusDays(0));

        String date3 = tahunAkademikMatrikulasi.getTanggalMulaiPembayaranString();
        String tahun3 = date3.substring(6, 10);
        String bulan3 = date3.substring(0, 2);
        String tanggal3 = date3.substring(3, 5);
        String tanggalan3 = tahun3 + '-' + bulan3 + '-' + tanggal3;
        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate3 = LocalDate.parse(tanggalan3, formatter3);
        tahunAkademikMatrikulasi.setTanggalMulaiPembayaran(localDate3.plusDays(0));

        String date4 = tahunAkademikMatrikulasi.getTanggalSelesaiPembayaranString();
        String tahun4 = date4.substring(6, 10);
        String bulan4 = date4.substring(0, 2);
        String tanggal4 = date4.substring(3, 5);
        String tanggalan4 = tahun4 + '-' + bulan4 + '-' + tanggal4;
        DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate4 = LocalDate.parse(tanggalan4, formatter4);
        tahunAkademikMatrikulasi.setTanggalSelesaiPembayaran(localDate4.plusDays(0));

        String date5 = tahunAkademikMatrikulasi.getTanggalMulaiPlotingDosenString();
        String tahun5 = date5.substring(6, 10);
        String bulan5 = date5.substring(0, 2);
        String tanggal5 = date5.substring(3, 5);
        String tanggalan5 = tahun5 + '-' + bulan5 + '-' + tanggal5;
        DateTimeFormatter formatter5 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate5 = LocalDate.parse(tanggalan5, formatter5);
        tahunAkademikMatrikulasi.setTanggalMulaiPlotingDosen(localDate5.plusDays(0));

        String date6 = tahunAkademikMatrikulasi.getTanggalSelesaiPlotingDosenString();
        String tahun6 = date6.substring(6, 10);
        String bulan6 = date6.substring(0, 2);
        String tanggal6 = date6.substring(3, 5);
        String tanggalan6 = tahun6 + '-' + bulan6 + '-' + tanggal6;
        DateTimeFormatter formatter6 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate6 = LocalDate.parse(tanggalan6, formatter6);
        tahunAkademikMatrikulasi.setTanggalSelesaiPlotingDosen(localDate6.plusDays(0));

        String date7 = tahunAkademikMatrikulasi.getTanggalMulaiPenjadwalanString();
        String tahun7 = date7.substring(6, 10);
        String bulan7 = date7.substring(0, 2);
        String tanggal7 = date7.substring(3, 5);
        String tanggalan7 = tahun7 + '-' + bulan7 + '-' + tanggal7;
        DateTimeFormatter formatter7 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate7 = LocalDate.parse(tanggalan7, formatter7);
        tahunAkademikMatrikulasi.setTanggalMulaiPenjadwalan(localDate7.plusDays(0));

        String date8 = tahunAkademikMatrikulasi.getTanggalSelesaiPenjadwalanString();
        String tahun8 = date8.substring(6, 10);
        String bulan8 = date8.substring(0, 2);
        String tanggal8 = date8.substring(3, 5);
        String tanggalan8 = tahun8 + '-' + bulan8 + '-' + tanggal8;
        DateTimeFormatter formatter8 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate8 = LocalDate.parse(tanggalan8, formatter8);
        tahunAkademikMatrikulasi.setTanggalSelesaiPenjadwalan(localDate8.plusDays(0));

        String date9 = tahunAkademikMatrikulasi.getTanggalMulaiKrsString();
        String tahun9 = date9.substring(6, 10);
        String bulan9 = date9.substring(0, 2);
        String tanggal9 = date9.substring(3, 5);
        String tanggalan9 = tahun9 + '-' + bulan9 + '-' + tanggal9;
        DateTimeFormatter formatter9 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate9 = LocalDate.parse(tanggalan9, formatter9);
        tahunAkademikMatrikulasi.setTanggalMulaiKrs(localDate9.plusDays(0));

        String date10 = tahunAkademikMatrikulasi.getTanggalSelesaiKrsString();
        String tahun10 = date10.substring(6, 10);
        String bulan10 = date10.substring(0, 2);
        String tanggal10 = date10.substring(3, 5);
        String tanggalan10 = tahun10 + '-' + bulan10 + '-' + tanggal10;
        DateTimeFormatter formatter10 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate10 = LocalDate.parse(tanggalan10, formatter10);
        tahunAkademikMatrikulasi.setTanggalSelesaiKrs(localDate10.plusDays(0));

        String date11 = tahunAkademikMatrikulasi.getTanggalMulaiKuliahString();
        String tahun11 = date11.substring(6, 10);
        String bulan11 = date11.substring(0, 2);
        String tanggal11 = date11.substring(3, 5);
        String tanggalan11 = tahun11 + '-' + bulan11 + '-' + tanggal11;
        DateTimeFormatter formatter11 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate11 = LocalDate.parse(tanggalan11, formatter11);
        tahunAkademikMatrikulasi.setTanggalMulaiKuliah(localDate11.plusDays(0));

        String date12 = tahunAkademikMatrikulasi.getTanggalSelesaiKuliahString();
        String tahun12 = date12.substring(6, 10);
        String bulan12 = date12.substring(0, 2);
        String tanggal12 = date12.substring(3, 5);
        String tanggalan12 = tahun12 + '-' + bulan12 + '-' + tanggal12;
        DateTimeFormatter formatter12 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate12 = LocalDate.parse(tanggalan12, formatter12);
        tahunAkademikMatrikulasi.setTanggalSelesaiKuliah(localDate12.plusDays(0));

        String date13 = tahunAkademikMatrikulasi.getTanggalMulaiUtsString();
        String tahun13 = date13.substring(6, 10);
        String bulan13 = date13.substring(0, 2);
        String tanggal13 = date13.substring(3, 5);
        String tanggalan13 = tahun13 + '-' + bulan13 + '-' + tanggal13;
        DateTimeFormatter formatter13 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate13 = LocalDate.parse(tanggalan13, formatter13);
        tahunAkademikMatrikulasi.setTanggalMulaiUts(localDate13.plusDays(0));

        String date14 = tahunAkademikMatrikulasi.getTanggalSelesaiUtsString();
        String tahun14 = date14.substring(6, 10);
        String bulan14 = date14.substring(0, 2);
        String tanggal14 = date14.substring(3, 5);
        String tanggalan14 = tahun14 + '-' + bulan14 + '-' + tanggal14;
        DateTimeFormatter formatter14 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate14 = LocalDate.parse(tanggalan14, formatter14);
        tahunAkademikMatrikulasi.setTanggalSelesaiUts(localDate14.plusDays(0));

        String date15 = tahunAkademikMatrikulasi.getTanggalMulaiUasString();
        String tahun15 = date15.substring(6, 10);
        String bulan15 = date15.substring(0, 2);
        String tanggal15 = date15.substring(3, 5);
        String tanggalan15 = tahun15 + '-' + bulan15 + '-' + tanggal15;
        DateTimeFormatter formatter15 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate15 = LocalDate.parse(tanggalan15, formatter15);
        tahunAkademikMatrikulasi.setTanggalMulaiUas(localDate15.plusDays(0));

        String date16 = tahunAkademikMatrikulasi.getTanggalSelesaiUasString();
        String tahun16 = date16.substring(6, 10);
        String bulan16 = date16.substring(0, 2);
        String tanggal16 = date16.substring(3, 5);
        String tanggalan16 = tahun16 + '-' + bulan16 + '-' + tanggal16;
        DateTimeFormatter formatter16 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate16 = LocalDate.parse(tanggalan16, formatter16);
        tahunAkademikMatrikulasi.setTanggalSelesaiUas(localDate16.plusDays(0));

        String date17 = tahunAkademikMatrikulasi.getTanggalMulaiPenilaianString();
        String tahun17 = date17.substring(6, 10);
        String bulan17 = date17.substring(0, 2);
        String tanggal17 = date17.substring(3, 5);
        String tanggalan17 = tahun17 + '-' + bulan17 + '-' + tanggal17;
        DateTimeFormatter formatter17 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate17 = LocalDate.parse(tanggalan17, formatter17);
        tahunAkademikMatrikulasi.setTanggalMulaiPenilaian(localDate17.plusDays(0));

        String date18 = tahunAkademikMatrikulasi.getTanggalSelesaiPenilaianString();
        String tahun18 = date18.substring(6, 10);
        String bulan18 = date18.substring(0, 2);
        String tanggal18 = date18.substring(3, 5);
        String tanggalan18 = tahun18 + '-' + bulan18 + '-' + tanggal18;
        DateTimeFormatter formatter18 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate18 = LocalDate.parse(tanggalan18, formatter18);
        tahunAkademikMatrikulasi.setTanggalSelesaiPenilaian(localDate18.plusDays(0));

        String date19 = tahunAkademikMatrikulasi.getTanggalSelesaiSemesterString();
        String tahun19 = date19.substring(6, 10);
        String bulan19 = date19.substring(0, 2);
        String tanggal19 = date19.substring(3, 5);
        String tanggalan19 = tahun19 + '-' + bulan19 + '-' + tanggal19;
        DateTimeFormatter formatter19 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate19 = LocalDate.parse(tanggalan19, formatter19);
        tahunAkademikMatrikulasi.setTanggalSelesaiSemester(localDate19.plusDays(0));

        Matrikulasi matrikulasi = matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1");

        tahunAkademikMatrikulasi.setMatrikulasi(matrikulasi);
        tahunAkademikMatrikulasiDao.save(tahunAkademikMatrikulasi);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:/setting/tahun_akademik_matrikulasi";
    }

    @PostMapping("setting/tahun_akademik_matrikulasi/hapus")
    public String deleteTahunAkademikMatrikulasi(@RequestParam(required = false) String tahunMatrikulasi,
                                           RedirectAttributes attributes) {

        TahunAkademikMatrikulasi tahunAkademikMatrikulasi = tahunAkademikMatrikulasiDao.findById(tahunMatrikulasi).get();
        tahunAkademikMatrikulasi.setStatus(StatusRecord.HAPUS);
        tahunAkademikMatrikulasiDao.save(tahunAkademikMatrikulasi);
        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/tahun_akademik_matrikulasi";

    }
}