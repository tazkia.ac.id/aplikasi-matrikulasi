package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.MatrikulasiDao;
import id.ac.tazkia.matrikulasi.dao.TahunAkademikDao;
import id.ac.tazkia.matrikulasi.dao.TahunAkademikTlcDao;
import id.ac.tazkia.matrikulasi.entity.Matrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import id.ac.tazkia.matrikulasi.entity.TahunAkademikTlc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class TahunAkademikTlcController {

    @Autowired private TahunAkademikTlcDao tahunAkademikTlcDao;
    @Autowired private TahunAkademikDao tahunAkademikDao;
    @Autowired private MatrikulasiDao matrikulasiDao;

    @GetMapping("setting/tahun_akademik_tlc")
    public String listTahunAkademikTlc(ModelMap modelMap, @PageableDefault Pageable pageable) {
        modelMap.addAttribute("tahunAkademikTlc", tahunAkademikTlcDao.findByStatusOrderByTanggalMulaiSemesterTlcDesc(StatusRecord.AKTIF, pageable));
        return "setting/tahun_akademik_tlc/list";
    }

    @GetMapping("setting/tahun_akademik_tlc/new")
    public String newTahunAkademikTlc(@RequestParam(required = false) String tahunAkademikTlc, ModelMap modelMap) {
        if (StringUtils.hasText(tahunAkademikTlc)) {
            modelMap.addAttribute("listTahunAkademik", tahunAkademikDao.findByStatusOrderByKodeTahunAkademik(StatusRecord.AKTIF));
            modelMap.addAttribute("tahunAkademikTlc", tahunAkademikTlcDao.findById(tahunAkademikTlc).get());
        } else {
            modelMap.addAttribute("listTahunAkademik", tahunAkademikDao.findByStatusOrderByKodeTahunAkademik(StatusRecord.AKTIF));
            modelMap.addAttribute("tahunAkademikTlc", new TahunAkademikTlc());
        }
        return "setting/tahun_akademik_tlc/form";
    }

    @PostMapping("/setting/tahun_akademik_tlc/new")
    public String saveTahunAkademikTlc(@ModelAttribute @Valid TahunAkademikTlc tahunAkademikTlc,
                                         RedirectAttributes attributes,
                                         BindingResult errors, ModelMap model) {

        String date = tahunAkademikTlc.getTanggalMulaiSemesterTlcString();
        String tahun = date.substring(6, 10);
        String bulan = date.substring(0, 2);
        String tanggal = date.substring(3, 5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        tahunAkademikTlc.setTanggalMulaiSemesterTlc(localDate.plusDays(0));

        String date1 = tahunAkademikTlc.getTanggalMulaiPenagihanTlcString();
        String tahun1 = date1.substring(6, 10);
        String bulan1 = date1.substring(0, 2);
        String tanggal1 = date1.substring(3, 5);
        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);
        tahunAkademikTlc.setTanggalMulaiPenagihanTlc(localDate1.plusDays(0));

        String date2 = tahunAkademikTlc.getTanggalSelesaiPenagihanTlcString();
        String tahun2 = date2.substring(6, 10);
        String bulan2 = date2.substring(0, 2);
        String tanggal2 = date2.substring(3, 5);
        String tanggalan2 = tahun2 + '-' + bulan2 + '-' + tanggal2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalan2, formatter2);
        tahunAkademikTlc.setTanggalSelesaiPenagihanTlc(localDate2.plusDays(0));

        String date3 = tahunAkademikTlc.getTanggalMulaiPembayaranTlcString();
        String tahun3 = date3.substring(6, 10);
        String bulan3 = date3.substring(0, 2);
        String tanggal3 = date3.substring(3, 5);
        String tanggalan3 = tahun3 + '-' + bulan3 + '-' + tanggal3;
        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate3 = LocalDate.parse(tanggalan3, formatter3);
        tahunAkademikTlc.setTanggalMulaiPembayaranTlc(localDate3.plusDays(0));

        String date4 = tahunAkademikTlc.getTanggalSelesaiPembayaranTlcString();
        String tahun4 = date4.substring(6, 10);
        String bulan4 = date4.substring(0, 2);
        String tanggal4 = date4.substring(3, 5);
        String tanggalan4 = tahun4 + '-' + bulan4 + '-' + tanggal4;
        DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate4 = LocalDate.parse(tanggalan4, formatter4);
        tahunAkademikTlc.setTanggalSelesaiPembayaranTlc(localDate4.plusDays(0));

        String date5 = tahunAkademikTlc.getTanggalMulaiPlotingDosenTlcString();
        String tahun5 = date5.substring(6, 10);
        String bulan5 = date5.substring(0, 2);
        String tanggal5 = date5.substring(3, 5);
        String tanggalan5 = tahun5 + '-' + bulan5 + '-' + tanggal5;
        DateTimeFormatter formatter5 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate5 = LocalDate.parse(tanggalan5, formatter5);
        tahunAkademikTlc.setTanggalMulaiPlotingDosenTlc(localDate5.plusDays(0));

        String date6 = tahunAkademikTlc.getTanggalSelesaiPlotingDosenTlcString();
        String tahun6 = date6.substring(6, 10);
        String bulan6 = date6.substring(0, 2);
        String tanggal6 = date6.substring(3, 5);
        String tanggalan6 = tahun6 + '-' + bulan6 + '-' + tanggal6;
        DateTimeFormatter formatter6 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate6 = LocalDate.parse(tanggalan6, formatter6);
        tahunAkademikTlc.setTanggalSelesaiPlotingDosenTlc(localDate6.plusDays(0));

        String date7 = tahunAkademikTlc.getTanggalMulaiPenjadwalanTlcString();
        String tahun7 = date7.substring(6, 10);
        String bulan7 = date7.substring(0, 2);
        String tanggal7 = date7.substring(3, 5);
        String tanggalan7 = tahun7 + '-' + bulan7 + '-' + tanggal7;
        DateTimeFormatter formatter7 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate7 = LocalDate.parse(tanggalan7, formatter7);
        tahunAkademikTlc.setTanggalMulaiPenjadwalanTlc(localDate7.plusDays(0));

        String date8 = tahunAkademikTlc.getTanggalSelesaiPenjadwalanTlcString();
        String tahun8 = date8.substring(6, 10);
        String bulan8 = date8.substring(0, 2);
        String tanggal8 = date8.substring(3, 5);
        String tanggalan8 = tahun8 + '-' + bulan8 + '-' + tanggal8;
        DateTimeFormatter formatter8 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate8 = LocalDate.parse(tanggalan8, formatter8);
        tahunAkademikTlc.setTanggalSelesaiPenjadwalanTlc(localDate8.plusDays(0));

        String date9 = tahunAkademikTlc.getTanggalMulaiKrsTlcString();
        String tahun9 = date9.substring(6, 10);
        String bulan9 = date9.substring(0, 2);
        String tanggal9 = date9.substring(3, 5);
        String tanggalan9 = tahun9 + '-' + bulan9 + '-' + tanggal9;
        DateTimeFormatter formatter9 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate9 = LocalDate.parse(tanggalan9, formatter9);
        tahunAkademikTlc.setTanggalMulaiKrsTlc(localDate9.plusDays(0));

        String date10 = tahunAkademikTlc.getTanggalSelesaiKrsTlcString();
        String tahun10 = date10.substring(6, 10);
        String bulan10 = date10.substring(0, 2);
        String tanggal10 = date10.substring(3, 5);
        String tanggalan10 = tahun10 + '-' + bulan10 + '-' + tanggal10;
        DateTimeFormatter formatter10 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate10 = LocalDate.parse(tanggalan10, formatter10);
        tahunAkademikTlc.setTanggalSelesaiKrsTlc(localDate10.plusDays(0));

        String date11 = tahunAkademikTlc.getTanggalMulaiKuliahTlcString();
        String tahun11 = date11.substring(6, 10);
        String bulan11 = date11.substring(0, 2);
        String tanggal11 = date11.substring(3, 5);
        String tanggalan11 = tahun11 + '-' + bulan11 + '-' + tanggal11;
        DateTimeFormatter formatter11 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate11 = LocalDate.parse(tanggalan11, formatter11);
        tahunAkademikTlc.setTanggalMulaiKuliahTlc(localDate11.plusDays(0));

        String date12 = tahunAkademikTlc.getTanggalSelesaiKuliahTlcString();
        String tahun12 = date12.substring(6, 10);
        String bulan12 = date12.substring(0, 2);
        String tanggal12 = date12.substring(3, 5);
        String tanggalan12 = tahun12 + '-' + bulan12 + '-' + tanggal12;
        DateTimeFormatter formatter12 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate12 = LocalDate.parse(tanggalan12, formatter12);
        tahunAkademikTlc.setTanggalSelesaiKuliahTlc(localDate12.plusDays(0));

        String date13 = tahunAkademikTlc.getTanggalMulaiUtsTlcString();
        String tahun13 = date13.substring(6, 10);
        String bulan13 = date13.substring(0, 2);
        String tanggal13 = date13.substring(3, 5);
        String tanggalan13 = tahun13 + '-' + bulan13 + '-' + tanggal13;
        DateTimeFormatter formatter13 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate13 = LocalDate.parse(tanggalan13, formatter13);
        tahunAkademikTlc.setTanggalMulaiUtsTlc(localDate13.plusDays(0));

        String date14 = tahunAkademikTlc.getTanggalSelesaiUtsTlcString();
        String tahun14 = date14.substring(6, 10);
        String bulan14 = date14.substring(0, 2);
        String tanggal14 = date14.substring(3, 5);
        String tanggalan14 = tahun14 + '-' + bulan14 + '-' + tanggal14;
        DateTimeFormatter formatter14 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate14 = LocalDate.parse(tanggalan14, formatter14);
        tahunAkademikTlc.setTanggalSelesaiUtsTlc(localDate14.plusDays(0));

        String date15 = tahunAkademikTlc.getTanggalMulaiUasTlcString();
        String tahun15 = date15.substring(6, 10);
        String bulan15 = date15.substring(0, 2);
        String tanggal15 = date15.substring(3, 5);
        String tanggalan15 = tahun15 + '-' + bulan15 + '-' + tanggal15;
        DateTimeFormatter formatter15 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate15 = LocalDate.parse(tanggalan15, formatter15);
        tahunAkademikTlc.setTanggalMulaiUasTlc(localDate15.plusDays(0));

        String date16 = tahunAkademikTlc.getTanggalSelesaiUasTlcString();
        String tahun16 = date16.substring(6, 10);
        String bulan16 = date16.substring(0, 2);
        String tanggal16 = date16.substring(3, 5);
        String tanggalan16 = tahun16 + '-' + bulan16 + '-' + tanggal16;
        DateTimeFormatter formatter16 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate16 = LocalDate.parse(tanggalan16, formatter16);
        tahunAkademikTlc.setTanggalSelesaiUasTlc(localDate16.plusDays(0));

        String date17 = tahunAkademikTlc.getTanggalMulaiPenilaianTlcString();
        String tahun17 = date17.substring(6, 10);
        String bulan17 = date17.substring(0, 2);
        String tanggal17 = date17.substring(3, 5);
        String tanggalan17 = tahun17 + '-' + bulan17 + '-' + tanggal17;
        DateTimeFormatter formatter17 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate17 = LocalDate.parse(tanggalan17, formatter17);
        tahunAkademikTlc.setTanggalMulaiPenilaianTlc(localDate17.plusDays(0));

        String date18 = tahunAkademikTlc.getTanggalSelesaiPenilaianTlcString();
        String tahun18 = date18.substring(6, 10);
        String bulan18 = date18.substring(0, 2);
        String tanggal18 = date18.substring(3, 5);
        String tanggalan18 = tahun18 + '-' + bulan18 + '-' + tanggal18;
        DateTimeFormatter formatter18 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate18 = LocalDate.parse(tanggalan18, formatter18);
        tahunAkademikTlc.setTanggalSelesaiPenilaianTlc(localDate18.plusDays(0));

        String date19 = tahunAkademikTlc.getTanggalSelesaiSemesterTlcString();
        String tahun19 = date19.substring(6, 10);
        String bulan19 = date19.substring(0, 2);
        String tanggal19 = date19.substring(3, 5);
        String tanggalan19 = tahun19 + '-' + bulan19 + '-' + tanggal19;
        DateTimeFormatter formatter19 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate19 = LocalDate.parse(tanggalan19, formatter19);
        tahunAkademikTlc.setTanggalSelesaiSemesterTlc(localDate19.plusDays(0));

        Matrikulasi matrikulasi = matrikulasiDao.findByStatusAndKampusId(StatusRecord.AKTIF, "1");

        tahunAkademikTlc.setMatrikulasi(matrikulasi);
        tahunAkademikTlcDao.save(tahunAkademikTlc);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:/setting/tahun_akademik_tlc";

    }

    @PostMapping("setting/tahun_akademik_tlc/hapus")
    public String deleteTahunAkademikTlc(@RequestParam(required = false) String akademikTlc,
                                           RedirectAttributes attributes) {

        TahunAkademikTlc tahunAkademikTlc = tahunAkademikTlcDao.findById(akademikTlc).get();
        tahunAkademikTlc.setStatus(StatusRecord.HAPUS);
        tahunAkademikTlcDao.save(tahunAkademikTlc);
        attributes.addFlashAttribute("deleted", "Save Data Berhasil");
        return "redirect:/setting/tahun_akademik_tlc";

    }
}
