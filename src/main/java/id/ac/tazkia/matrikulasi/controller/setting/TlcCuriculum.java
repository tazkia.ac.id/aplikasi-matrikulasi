package id.ac.tazkia.matrikulasi.controller.setting;


import id.ac.tazkia.matrikulasi.dao.KurikulumTlcDao;
import id.ac.tazkia.matrikulasi.entity.KurikulumTlc;
import id.ac.tazkia.matrikulasi.entity.MatakuliahTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class TlcCuriculum {

    @Autowired
    private KurikulumTlcDao kurikulumTlcDao;

    @GetMapping("setting/tlc_curiculum")
    public String tlcCurilum(Model m,
                            @RequestParam(required = false) String search,
                            @PageableDefault(size = 10) Pageable page) {
        if(StringUtils.hasText(search)) {
            m.addAttribute("search", search);
            m.addAttribute("listKurikulumTlc", kurikulumTlcDao.findByNamaKurikulumContainingIgnoreCaseOrderByKodeKurikulumDesc(search,page));
        }else {
            m.addAttribute("listKurikulumTlc", kurikulumTlcDao.findAll(page));
        }
        return "setting/tlc_curiculum/list";

    }

    @GetMapping("setting/tlc_curiculum/new")
    public String tlcCurilumNew(@RequestParam(value = "id", required = false) String id,
                                Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("kurikulumTlc", new KurikulumTlc());

        if (id != null && !id.isEmpty()){
            KurikulumTlc p= kurikulumTlcDao.findById(id).get();
            if (p != null){
                m.addAttribute("kurikulumTlc", p);
            }
        }
        return "setting/tlc_curiculum/form";

    }

    @PostMapping("setting/tlc_curiculum/save")
    public String saveTlcCuriculum(@Valid @ModelAttribute KurikulumTlc kurikulumTlc,
                                RedirectAttributes attributes){


        kurikulumTlcDao.save(kurikulumTlc);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../tlc_curiculum";

    }


}
