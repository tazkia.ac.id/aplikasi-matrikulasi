package id.ac.tazkia.matrikulasi.controller.setting;

import id.ac.tazkia.matrikulasi.dao.KurikulumTlcDao;
import id.ac.tazkia.matrikulasi.dao.MatakuliahKurikulumTlcDao;
import id.ac.tazkia.matrikulasi.dao.MatakuliahTlcDao;
import id.ac.tazkia.matrikulasi.entity.KurikulumTlc;
import id.ac.tazkia.matrikulasi.entity.MatakuliahKurikulumTlc;
import id.ac.tazkia.matrikulasi.entity.MatakuliahTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class TlcCuriculumCourse {

    @Autowired
    private KurikulumTlcDao kurikulumTlcDao;

    @Autowired
    private MatakuliahTlcDao matakuliahTlcDao;

    @Autowired
    private MatakuliahKurikulumTlcDao matakuliahKurikulumTlcDao;

    @GetMapping("setting/tlc_curiculum/course")
    public String settingTlcCuriculumCourse(Model model,
                                            @RequestParam(required = false)KurikulumTlc kurikulumTlc) {

        if (kurikulumTlc != null){
            model.addAttribute("selectedKurikulumTlc", kurikulumTlc);
            model.addAttribute("listMatakuliahTlc", matakuliahTlcDao.findByStatusOrderByNamaMatakuliah(StatusRecord.AKTIF));
            model.addAttribute("listMatakuliahKurikulumTlc", matakuliahKurikulumTlcDao.findByStatusAndKurikulumTlcOrderBySemester(StatusRecord.AKTIF, kurikulumTlc));
        }
        model.addAttribute("listKurikulumTlc", kurikulumTlcDao.findByStatusOrderByNamaKurikulum(StatusRecord.AKTIF));
        model.addAttribute("kurikulumTlc", new KurikulumTlc());
        model.addAttribute("matakuliahKurikulumTlc", new MatakuliahKurikulumTlc());

        return"setting/tlc_curiculum_course/list";

    }

    @PostMapping("setting/tlc_curiculum/course/save")
    public String saveCuriculumCourseTlc(@Valid @ModelAttribute MatakuliahKurikulumTlc matakuliahKurikulumTlc,
                                         RedirectAttributes attributes,
                                         @RequestParam(required = false) KurikulumTlc kurikulumTlc){

        matakuliahKurikulumTlc.setKurikulumTlc(kurikulumTlc);
        matakuliahKurikulumTlcDao.save(matakuliahKurikulumTlc);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../course?kurikulumTlc="+ kurikulumTlc.getId() +"&search=";
    }

}
