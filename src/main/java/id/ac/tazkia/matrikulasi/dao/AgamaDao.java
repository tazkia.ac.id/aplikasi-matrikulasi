package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Agama;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
//import jdk.internal.dynalink.linker.LinkerServices;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgamaDao extends PagingAndSortingRepository<Agama, String> {

    List<Agama> findByStatusOrderByAgamaAsc(StatusRecord status);

}
