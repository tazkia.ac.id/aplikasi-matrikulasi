package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Fakultas;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FakultasDao extends PagingAndSortingRepository<Fakultas, String> {
}
