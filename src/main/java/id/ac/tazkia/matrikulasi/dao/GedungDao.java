package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Gedung;
import id.ac.tazkia.matrikulasi.entity.JenisRuang;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GedungDao extends PagingAndSortingRepository<Gedung, String> {

    Page<Gedung> findByStatusOrderByNamaGedungDesc(StatusRecord status, Pageable pageable);

}
