package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.JenisKewajiban;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JenisKewajibanDao extends PagingAndSortingRepository<JenisKewajiban, String> {
    List<JenisKewajiban> findByStatusOrderByNamaJenisKewajibanAsc(StatusRecord status, Pageable page);
    Page<JenisKewajiban> findByStatus(StatusRecord status, Pageable pageable);
}
