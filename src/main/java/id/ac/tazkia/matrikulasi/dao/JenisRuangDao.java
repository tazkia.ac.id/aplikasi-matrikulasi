package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.JenisRuang;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JenisRuangDao extends PagingAndSortingRepository <JenisRuang, String> {
    Page<JenisRuang> findByStatus(StatusRecord statusRecord, Pageable pageable);
}
