package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Kampus;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KampusDao extends PagingAndSortingRepository<Kampus, String> {

    Kampus findByStatusAndId(StatusRecord statusRecord, String id);

}
