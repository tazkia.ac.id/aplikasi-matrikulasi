package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Karyawan;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KaryawanDao extends PagingAndSortingRepository<Karyawan, String> {

    Page<Karyawan> findByStatusOrderByNamaKaryawanAsc(StatusRecord statusRecord, Pageable page);

    @Query("SELECT k FROM Karyawan k WHERE k.status = 'AKTIF' AND CONCAT(k.namaKaryawan, k.nik, k.jenisKelamin)" +
            " LIKE %?1%")
    Page<Karyawan> findByStatusAndSearch(String search,StatusRecord statusRecord, Pageable pageable);

}
