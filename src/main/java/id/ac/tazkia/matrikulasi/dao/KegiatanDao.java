package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Kegiatan;
import id.ac.tazkia.matrikulasi.entity.PenjadwalanKegiatanTemporer;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KegiatanDao extends PagingAndSortingRepository<Kegiatan, String> {

    Page<Kegiatan> findByStatusOrderByNamaKegiatanAsc(StatusRecord statusRecord, Pageable pageable);

    List<Kegiatan> findByStatus(StatusRecord statusRecord);

    Kegiatan findByStatusAndId(StatusRecord status, String id);
}
