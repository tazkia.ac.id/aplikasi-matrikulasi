package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.dto.JenisKelaminDto;
import id.ac.tazkia.matrikulasi.entity.Kelas;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KelasDao extends PagingAndSortingRepository<Kelas, String> {

    @Query(value = "select jenis_kelamin as jenisKelamin from kelas where status = 'AKTIF' GROUP BY jenis_kelamin order by jenis_kelamin desc", nativeQuery = true)
    List<JenisKelaminDto> listJenisKelamin();



}