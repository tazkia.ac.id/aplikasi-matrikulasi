package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.KurikulumTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KurikulumTlcDao extends PagingAndSortingRepository<KurikulumTlc, String> {

    List<KurikulumTlc> findByStatusOrderByNamaKurikulum(StatusRecord statusRecord);

    Page<KurikulumTlc> findByNamaKurikulumContainingIgnoreCaseOrderByKodeKurikulumDesc(String nama,Pageable pageable);
}
