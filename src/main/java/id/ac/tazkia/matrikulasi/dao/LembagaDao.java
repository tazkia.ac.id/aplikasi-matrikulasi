package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Lembaga;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LembagaDao extends PagingAndSortingRepository<Lembaga, String> {
    List<Lembaga> findByStatusOrderByNamaLembagaDesc(StatusRecord status);

    Lembaga findByStatusAndId(StatusRecord status, String id);
}
