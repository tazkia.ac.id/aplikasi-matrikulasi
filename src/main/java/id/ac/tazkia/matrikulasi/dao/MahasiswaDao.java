package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.dto.AngkatanDto;
import id.ac.tazkia.matrikulasi.dto.JenisKelaminDto;
import id.ac.tazkia.matrikulasi.dto.MahasiswaDto;
import id.ac.tazkia.matrikulasi.entity.Mahasiswa;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MahasiswaDao extends PagingAndSortingRepository<Mahasiswa, String> {

    Page<Mahasiswa> findByStatusOrderByNimDesc(StatusRecord status, Pageable page);

    @Query(value = "select angkatan as angkatan from mahasiswa where status = 'AKTIF' group by angkatan order by angkatan desc", nativeQuery = true)
    List<AngkatanDto> listAngkatan();

    @Query(value = "SELECT m.id AS id, m.nama AS nama, m.nim AS nim, m.jenisKelamin AS jenisKelamin, m.prodi.namaProdi AS namaProdi, \n" +
            "p.id FROM Mahasiswa AS m INNER JOIN Prodi AS p \n" +
            "ON m.prodi = p.id WHERE m.status = 'AKTIF' AND p.status = 'AKTIF' ORDER BY m.jenisKelamin")
    Page<MahasiswaDto> listMahasiswa(Pageable pageable);

}