package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.KurikulumTlc;
import id.ac.tazkia.matrikulasi.entity.MatakuliahKurikulumTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MatakuliahKurikulumTlcDao extends PagingAndSortingRepository<MatakuliahKurikulumTlc, String> {

    List<MatakuliahKurikulumTlc> findByStatusAndKurikulumTlcOrderBySemester(StatusRecord statusRecord, KurikulumTlc kurikulumTlc);

}
