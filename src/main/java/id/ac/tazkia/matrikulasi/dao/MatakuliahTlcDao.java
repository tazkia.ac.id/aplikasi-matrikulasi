package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.MatakuliahTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MatakuliahTlcDao extends PagingAndSortingRepository<MatakuliahTlc, String> {

    List<MatakuliahTlc> findByStatusOrderByNamaMatakuliah(StatusRecord statusRecord);

    Page<MatakuliahTlc> findByStatusOrderByNamaMatakuliah(StatusRecord statusRecord, Pageable page);

}
