package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Matrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatrikulasiDao extends PagingAndSortingRepository<Matrikulasi, String> {

    Matrikulasi findByStatus(StatusRecord status);
    Matrikulasi findByStatusAndKampusId(StatusRecord statusRecord, String id);
}