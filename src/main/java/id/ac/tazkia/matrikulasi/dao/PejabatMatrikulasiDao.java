package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.PejabatMatrikulasi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;

public interface PejabatMatrikulasiDao extends PagingAndSortingRepository<PejabatMatrikulasi, String> {

    Page<PejabatMatrikulasi> findByStatusOrderByTanggalMulaiDesc(StatusRecord statusRecord, Pageable page);
    Page<PejabatMatrikulasi> findByStatus(StatusRecord statusRecord, Pageable page);


}
