package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.PenjadwalanKegiatanRutin;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface PenjadwalanKegiatanRutinDao extends PagingAndSortingRepository<PenjadwalanKegiatanRutin, String> {

    Page<PenjadwalanKegiatanRutin> findByStatus(StatusRecord aktif, Pageable pageable);

    PenjadwalanKegiatanRutin findByJamMulai(Date jamMulai);

    PenjadwalanKegiatanRutin findByStatusAndId(StatusRecord statusRecord, String id);

    @Query("SELECT p FROM PenjadwalanKegiatanRutin p WHERE p.status='AKTIF' AND " +
            "CONCAT(p.deskripsiKegiatan, p.jamMulai, p.jenisKelamin, p.tanggalPelaksanaan) LIKE %?1%")
    Page<PenjadwalanKegiatanRutin> findByStatusAndSearch(String search, StatusRecord aktif, Pageable pageable);

}
