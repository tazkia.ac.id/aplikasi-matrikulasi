package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Mahasiswa;
import id.ac.tazkia.matrikulasi.entity.PenjadwalanKegiatanTemporer;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.io.StringReader;
import java.util.Date;

@Repository
public interface PenjadwalanKegiatanTemporerDao extends PagingAndSortingRepository<PenjadwalanKegiatanTemporer, String> {

    PenjadwalanKegiatanTemporer findByRuangan (String ruangan);

    PenjadwalanKegiatanTemporer findByJamMulai (Date jamMulai);

    Page<PenjadwalanKegiatanTemporer> findByStatus(StatusRecord statusRecord, Pageable pageable);

    PenjadwalanKegiatanTemporer findByStatusAndTanggalKegiatanString (StatusRecord statusRecord, String tanggalKegiatanString);

    @Query("SELECT p FROM PenjadwalanKegiatanTemporer p WHERE " + "CONCAT(p.deksripsi ,p.jamMulai, p.jenisKelamin, p.jamMulai, p.jamSelesai) " +
            " LIKE %?1%")
    Page<PenjadwalanKegiatanTemporer> findByStatusOrderByTanggalKegiatan(StatusRecord statusRecord, Pageable pageable, String search);

    @Query(value = "SELECT a.id, a.kegiatan.namaKegiatan, a.jenisKelamin, a.tanggalKegiatanString, a.jamMulai, \n" +
            "b.id FROM PenjadwalanKegiatanTemporer AS a INNER JOIN Kegiatan AS b \n" +
            "ON a.kegiatan = b.id WHERE a.status = 'AKTIF' AND b.status = 'AKTIF' ORDER BY a.jamMulai")
    Page<Object> listKegiatanTemporer(Pageable pageable);

    @Query(value = "SELECT a FROM Mahasiswa AS a WHERE a.status='AKTIF' ORDER BY a.nik")
    Page<Mahasiswa> listAbsensiMahasiswa(Pageable pageable);

}