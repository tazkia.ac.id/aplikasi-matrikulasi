package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.JenisKelamin;
import id.ac.tazkia.matrikulasi.entity.PlotingKelas;
import id.ac.tazkia.matrikulasi.entity.Ruangan;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
//import sun.nio.cs.ext.IBM037;

import java.util.List;

public interface PlotingKelasDao extends PagingAndSortingRepository<PlotingKelas, String> {


    @Query(value = "SELECT id, kode_kelas, nama_kelas, COALESCE(jml,0) AS jumlah FROM \n" +
            "(SELECT id, kode_kelas, nama_kelas FROM kelas WHERE STATUS='AKTIF' AND jenis_kelamin = ?2) p\n" +
            "LEFT JOIN \n" +
            "(SELECT COUNT(id_kelas)AS jml, id_kelas FROM ploting_kelas AS p \n" +
            "INNER JOIN mahasiswa AS m ON p.id_mahasiswa = m.id \n" +
            "WHERE p.status = 'AKTIF' AND m.status='AKTIF' AND m.jenis_kelamin = ?2 AND m.angkatan = ?1\n" +
            "GROUP BY id_kelas) m ON p.id = m.id_kelas", nativeQuery = true)
    List<Object> listPlotingKelas(String angkatan, String jenisKelamin);
    

    @Query(value = "SELECT id, kode_kelas, nama_kelas, COALESCE(jml,0) AS jumlah FROM \n" +
            "(SELECT id, kode_kelas, nama_kelas FROM kelas WHERE STATUS='AKTIF') p\n" +
            "LEFT JOIN \n" +
            "(SELECT COUNT(id_kelas)AS jml, id_kelas FROM ploting_kelas AS p \n" +
            "INNER JOIN mahasiswa AS m ON p.id_mahasiswa = m.id \n" +
            "WHERE p.status = 'AKTIF' AND m.status='AKTIF' AND m.angkatan = ?1\n" +
            "GROUP BY id_kelas) m ON p.id = m.id_kelas", nativeQuery = true)
    List<Object> listPlotingKelasTanpaKenisKelamin(String angkatan);

//    Page<PlotingKelas> findByStatusAndKelasJenisKelaminAndMahasiswaAngkatan(StatusRecord statusRecord, JenisKelamin jenisKelamin, String angkatan, Pageable pageable);
//
//    Page<PlotingKelas> findByStatusAndMahasiswaAngkatan(StatusRecord statusRecord, String angkatan, Pageable pageable);


    @Query(value = "SELECT a.id, a.angkatan, a.nim, a.nama, COALESCE(b.nama_kelas, 'Belum di plot') AS namaKelas FROM \n" +
            "(SELECT id, angkatan, nim, nama FROM mahasiswa WHERE STATUS = 'AKTIF' AND angkatan = ?1 AND jenis_kelamin = ?2) a\n" +
            "LEFT JOIN \n" +
            "(SELECT b.id_mahasiswa, k.* FROM ploting_kelas AS b\n" +
            "LEFT JOIN \n" +
            "kelas AS k ON b.id_kelas = k.id WHERE b.status='AKTIF' AND k.status = 'AKTIF')b \n" +
            "ON a.id = b.id_mahasiswa \n" +
            "ORDER BY nim\n", nativeQuery = true)

    List<Object> listPlotingMahasiswa(String angkatan, String jenisKelamin);

    List<PlotingKelas> findByStatusAndMahasiswaId(StatusRecord statusRecord, String idMahasiswa);

}