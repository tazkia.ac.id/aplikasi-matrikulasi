package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.PlotingPembina;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PlotingPembinaDao extends PagingAndSortingRepository<PlotingPembina, String> {

    Page<PlotingPembina> findByStatusOrderByMahasiswaNimAsc(StatusRecord statusRecord, Pageable pageable);

    Page<PlotingPembina> findByStatusAndMahasiswaStatusAndPembinaStatusOrderByMahasiswaNimAsc(StatusRecord statusPloting, StatusRecord statusMahasiswa, StatusRecord statusPembina, Pageable pageable);

    List<PlotingPembina> findByStatusAndMahasiswaId(StatusRecord statusRecord, String idMahasiswa);

    Page<PlotingPembina> findByStatusAndPembinaId(StatusRecord statusRecord, String idPembina, Pageable pageable);

    @Query(value = "SELECT id,nama_karyawan, COALESCE(jml,0)AS jumlah FROM \n" +
            "(SELECT id,nama_karyawan FROM karyawan WHERE STATUS='AKTIF' AND jenis_kelamin = ?2 AND pembina='AKTIF')a\n" +
            "LEFT JOIN \n" +
            "(SELECT COUNT(id_pembina)AS jml, id_pembina FROM ploting_pembina AS a \n" +
            "INNER JOIN mahasiswa AS b ON a.id_mahasiswa = b.id \n" +
            "WHERE a.status = 'AKTIF' AND b.status='AKTIF' AND b.jenis_kelamin = ?2 AND b.angkatan = ?1\n" +
            "GROUP BY id_pembina) b ON a.id = b.id_pembina", nativeQuery = true)
    List<Object> listPlotingPembina(String angkatan, String jenisKelamin);


    @Query(value = "SELECT a.id, a.angkatan, a.nim, a.nama, COALESCE(b.nama_karyawan,'Belum di plot') AS pembina FROM\n" +
            "(SELECT id, angkatan, nim, nama FROM mahasiswa WHERE STATUS = 'AKTIF' AND angkatan = ?1 AND jenis_kelamin = ?2) a\n" +
            "LEFT JOIN \n" +
            "(SELECT b.id_mahasiswa,c.* FROM ploting_pembina AS b\n" +
            "LEFT JOIN \n" +
            "karyawan AS c ON b.id_pembina = c.id WHERE b.status='AKTIF' AND c.status = 'AKTIF' AND c.jenis_kelamin = ?2)b\n" +
            " ON a.id = b.id_mahasiswa \n" +
            "ORDER BY nim\n", nativeQuery = true)
    List<Object> listPlotingMahasiswa(String angkatan, String jenisKelamin);

}
