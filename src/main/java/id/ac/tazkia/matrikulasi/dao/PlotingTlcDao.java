package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.PlotingTlc;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface  PlotingTlcDao extends PagingAndSortingRepository<PlotingTlc, String> {

    @Query(value = "SELECT id, kode_kelas_tlc, nama_kelas_tlc, COALESCE(jml,0) AS jumlah FROM \n" +
            "(SELECT id, kode_kelas_tlc, nama_kelas_tlc FROM kelas_tlc WHERE STATUS='AKTIF' AND jenis_kelamin = ?2) p\n" +
            "LEFT JOIN \n" +
            "(SELECT COUNT(id_kelas_tlc)AS jml, id_kelas_tlc FROM ploting_tlc AS p \n" +
            "INNER JOIN mahasiswa AS m ON p.id_mahasiswa = m.id \n" +
            "WHERE p.status = 'AKTIF' AND m.status='AKTIF' AND m.jenis_kelamin = ?2 AND m.angkatan = ?1\n" +
            "GROUP BY id_kelas_tlc) m ON p.id = m.id_kelas_tlc", nativeQuery = true)
    List<Object> listPlotingTlc(String angkatan, String jenisKelamin);

    @Query(value = "SELECT id, kode_kelas, nama_kelas, COALESCE(jml,0) AS jumlah FROM \n" +
            "(SELECT id, kode_kelas, nama_kelas FROM kelas WHERE STATUS='AKTIF') p\n" +
            "LEFT JOIN \n" +
            "(SELECT COUNT(id_kelas)AS jml, id_kelas FROM ploting_tlc AS p \n" +
            "INNER JOIN mahasiswa AS m ON p.id_mahasiswa = m.id \n" +
            "WHERE p.status = 'AKTIF' AND m.status='AKTIF' AND m.angkatan = ?1\n" +
            "GROUP BY id_kelas) m ON p.id = m.id_kelas", nativeQuery = true)
    List<Object> listPlotingTlcTanpaJenisKelamin(String angkatan);

    @Query(value = "SELECT a.id, a.angkatan, a.nim, a.nama, COALESCE(b.nama_kelas_tlc, 'Belum di plot') AS namaKelasTlc FROM \n" +
            "(SELECT id, angkatan, nim, nama FROM mahasiswa WHERE STATUS = 'AKTIF' AND angkatan = ?1 AND jenis_kelamin = ?2) a\n" +
            "LEFT JOIN \n" +
            "(SELECT b.id_mahasiswa, k.* FROM ploting_tlc AS b\n" +
            "LEFT JOIN \n" +
            "kelas_tlc AS k ON b.id_kelas_tlc = k.id WHERE b.status='AKTIF' AND k.status = 'AKTIF')b \n" +
            "ON a.id = b.id_mahasiswa \n" +
            "ORDER BY nim\n", nativeQuery = true)
    List<Object> listPlotingMahasiswa(String angkatan, String jenisKelamin);

    List<PlotingTlc> findByStatusAndMahasiswaId(StatusRecord statusRecord, String idMahasiswa);
}