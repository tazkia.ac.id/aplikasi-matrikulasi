package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Karyawan;
import id.ac.tazkia.matrikulasi.entity.PresensiDosenKegiatanTemporer;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PresensiDosenKegiatanTemporerDao extends PagingAndSortingRepository <PresensiDosenKegiatanTemporer, String> {

    List<PresensiDosenKegiatanTemporer> findByStatusAndKegiatanTemporerId(StatusRecord statusRecord, String idPenjadwalan);

    PresensiDosenKegiatanTemporer findByStatusAndKaryawanId(StatusRecord statusRecord, String karyawan);

    PresensiDosenKegiatanTemporer findByStatusAndKaryawanIdAndKegiatanTemporerId(StatusRecord statusRecord, String karyawan, String kegiatan);
}