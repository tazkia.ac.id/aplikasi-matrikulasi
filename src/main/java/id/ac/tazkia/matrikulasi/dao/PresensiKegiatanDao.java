package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.PresensiKegiatanRutin;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface PresensiKegiatanDao extends PagingAndSortingRepository<PresensiKegiatanRutin, String> {

    List<PresensiKegiatanRutin> findByStatusAndPenjadwalanKegiatanRutinId(StatusRecord statusRecord, String idPenjadwalan);

    PresensiKegiatanRutin findByStatusAndKaryawanIdAndPenjadwalanKegiatanRutinId(StatusRecord statusRecord, String idKaryawan, String idPenjadwalan );

}
