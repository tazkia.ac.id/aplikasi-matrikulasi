package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Mahasiswa;
import id.ac.tazkia.matrikulasi.entity.PresensiMahasiswaKegiatanRutin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PresensiMahasiswaKegiatanRutinDao extends PagingAndSortingRepository<PresensiMahasiswaKegiatanRutin, String> {

    @Query(value = "select m from Mahasiswa as m where m.status='aktif' order by m.nik")
    Page<Mahasiswa> listPresensiMahasiswa(Pageable pageable);

//    @Query(value = "SELECT a.id, a.nim, a.nama, b.nama_kelas FROM \n" +
//            "(SELECT id, nim, nama FROM mahasiswa WHERE STATUS='AKTIF')a\n" +
//            "LEFT JOIN \n" +
//            "(SELECT b.id_mahasiswa, k.* FROM presensi_mahasiswa_kegiatan_rutin AS b\n" +
//            "LEFT JOIN \n " +
//            "kelas AS k ON b.id_kelas = k.id WHERE b.status = 'AKTIF' AND k.status = 'AKTIF')b\n" +
//            "ON a.id = b.id_mahasiswa \n" +
//            "ORDER BY nim ", nativeQuery = true)
//    Page<Object> listPresensiMahasiswa(Pageable pageable);
//

}
