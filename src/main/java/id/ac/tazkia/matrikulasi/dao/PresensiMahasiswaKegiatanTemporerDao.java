package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PresensiMahasiswaKegiatanTemporerDao extends PagingAndSortingRepository<PresensiMahasiswaKegiatanTemporer, String> {

    @Query(value = "SELECT a.id, a.nim, a.nama, b.nama_kelas FROM \n" +
            "(SELECT id, nim, nama FROM mahasiswa WHERE STATUS='AKTIF')a\n" +
            "LEFT JOIN \n" +
            "(SELECT b.id_mahasiswa, k.* FROM absen_mahasiswa AS b\n" +
            "LEFT JOIN \n " +
            "kelas AS k ON b.id_kelas = k.id WHERE b.status = 'AKTIF' AND k.status = 'AKTIF')b\n" +
            "ON a.id = b.id_mahasiswa \n" +
            "ORDER BY nim ASC ", nativeQuery = true)
    Page<Object> listMahasiswa(Pageable pageable);
}