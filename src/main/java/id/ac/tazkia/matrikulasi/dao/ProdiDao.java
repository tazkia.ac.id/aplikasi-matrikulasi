package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.Prodi;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
//import sun.jvm.hotspot.debugger.Page;

import java.util.List;

@Repository
public interface ProdiDao extends PagingAndSortingRepository<Prodi, String> {

    List<Prodi> findByStatusOrderByKodeProdiAsc(StatusRecord statusRecord);

}
