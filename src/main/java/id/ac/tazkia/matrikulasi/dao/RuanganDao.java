package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.dto.KelasDto;
import id.ac.tazkia.matrikulasi.dto.KodeKelasDto;
import id.ac.tazkia.matrikulasi.entity.Ruangan;
import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface    RuanganDao extends PagingAndSortingRepository<Ruangan, String> {

    Page<Ruangan> findByStatus(StatusRecord status, Pageable pageable);

    @Query(value = "select namaRuang as namaRuang from ruangan where status = 'AKTIF' group by namaRuang order by namaRuang desc", nativeQuery = true)
    List<KelasDto> listKelas();

    @Query(value = "select kodeRuang as kodeRuang from ruangan where status = 'AKTIF' group by kodeRuang order by kodeRuang desc", nativeQuery = true)
    List<KodeKelasDto> listKodeKelas();
}
