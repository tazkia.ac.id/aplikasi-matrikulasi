package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import id.ac.tazkia.matrikulasi.entity.TahunAkademik;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TahunAkademikDao extends PagingAndSortingRepository<TahunAkademik, String> {

    List<TahunAkademik> findByStatusOrderByKodeTahunAkademik(StatusRecord statusRecord);

}
