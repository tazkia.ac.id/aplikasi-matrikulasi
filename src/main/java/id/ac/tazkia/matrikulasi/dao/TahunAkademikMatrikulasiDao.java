package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import id.ac.tazkia.matrikulasi.entity.TahunAkademikMatrikulasi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TahunAkademikMatrikulasiDao extends PagingAndSortingRepository<TahunAkademikMatrikulasi, String> {

    Page<TahunAkademikMatrikulasi> findByStatusOrderByTanggalMulaiSemesterDesc(StatusRecord statusRecord, Pageable page);
//    Page<TahunAkademikMatrikulasi> findByStatus(StatusRecord statusRecord, Pageable pageable);

}
