package id.ac.tazkia.matrikulasi.dao;

import id.ac.tazkia.matrikulasi.entity.StatusRecord;
import id.ac.tazkia.matrikulasi.entity.TahunAkademikTlc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TahunAkademikTlcDao extends PagingAndSortingRepository<TahunAkademikTlc, String> {

    Page<TahunAkademikTlc> findByStatusOrderByTanggalMulaiSemesterTlcDesc(StatusRecord statusRecord, Pageable page);

}
