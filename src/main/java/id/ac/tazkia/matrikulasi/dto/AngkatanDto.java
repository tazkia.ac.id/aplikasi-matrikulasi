package id.ac.tazkia.matrikulasi.dto;

public interface AngkatanDto {
    String getAngkatan();
}
