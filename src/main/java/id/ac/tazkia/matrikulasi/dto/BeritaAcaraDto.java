package id.ac.tazkia.matrikulasi.dto;

import id.ac.tazkia.matrikulasi.entity.Karyawan;
import id.ac.tazkia.matrikulasi.entity.PenjadwalanKegiatanTemporer;
import lombok.Data;

@Data
public class BeritaAcaraDto {

    private String id;
    private String pemateri;
    private String beritaAcara;
    private Karyawan karyawan;
    private PenjadwalanKegiatanTemporer penjadwalanKegiatanTemporer;

}