package id.ac.tazkia.matrikulasi.dto;

public interface JenisKelaminDto {

    String getJenisKelamin();

}
