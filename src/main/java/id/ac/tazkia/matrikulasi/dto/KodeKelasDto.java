package id.ac.tazkia.matrikulasi.dto;

public interface KodeKelasDto {
    String getKodeRuang();
}
