package id.ac.tazkia.matrikulasi.dto;

public interface MahasiswaDto {

    String getId();
    String getNama();
    String getNim();
    String getJenisKelamin();
    String getNamaProdi();
}
