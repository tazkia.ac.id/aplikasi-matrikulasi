package id.ac.tazkia.matrikulasi.dto;

import id.ac.tazkia.matrikulasi.entity.JenisKelamin;
import lombok.Data;

@Data
public class SearchGenderDto {

    private String jenisKelamin;
    private String angkatan;

}
