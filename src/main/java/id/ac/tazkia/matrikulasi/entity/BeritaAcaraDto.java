package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

@Data
public class BeritaAcaraDto {

    private String pemateri;
    private String beritaAcara;
    private Karyawan karyawan;
    private PenjadwalanKegiatanRutin penjadwalanKegiatanRutin;

}
