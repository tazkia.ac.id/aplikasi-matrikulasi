package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Fakultas extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_lembaga")
    private Lembaga lembaga;

    private String kodeFakultas;
    private String namaFakultas;
    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
