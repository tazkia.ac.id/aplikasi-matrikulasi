package id.ac.tazkia.matrikulasi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.validation.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@Table
public class Gedung extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty(message = "Nama gedung harus di isi")
    private String namaGedung;

    private int jumlahLantai;

    @NotNull
    @Column(nullable = false, length = 5)
    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalInsert = new Date();

    private String userEdit;

    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalEdit = new Date();

    private String userDelete;

    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalDelete = new Date();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_matrikulasi")
    private Matrikulasi matrikulasi;

//    @JsonIgnore
//    @OneToMany(mappedBy = "gedung", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<Ruangan> ruanganSet = new HashSet<>();

}