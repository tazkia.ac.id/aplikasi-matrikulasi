package id.ac.tazkia.matrikulasi.entity;

public enum JenisKelamin {

    PRIA, WANITA, GABUNGAN

}