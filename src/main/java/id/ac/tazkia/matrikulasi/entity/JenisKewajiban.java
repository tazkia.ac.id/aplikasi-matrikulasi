package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Data
@Entity
@Table
public class JenisKewajiban extends BaseEntity {

    @NotEmpty(message = "Kode Jenis Kewajiban tidak boleh kosong!")
    private String kodeJenisKewajiban;

    @NotEmpty(message = "Nama Jenis Kewajiban tidak boleh kosong!")
    private String namaJenisKewajiban;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
