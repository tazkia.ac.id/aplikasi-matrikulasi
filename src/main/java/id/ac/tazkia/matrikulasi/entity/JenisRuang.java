package id.ac.tazkia.matrikulasi.entity;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
@Table
public class JenisRuang extends BaseEntity{

    @NotEmpty(message = "Kode Jenis Ruang tidak boleh kosong!")
    private String kodeJenisRuangan;

    @NotEmpty(message = "Jenis ruang tidak boleh kosong!")
    private String jenisRuangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
