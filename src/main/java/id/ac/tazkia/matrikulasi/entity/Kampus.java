package id.ac.tazkia.matrikulasi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Data
@Table
public class Kampus {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String kodeKampus;
    private String namaKampus;
    private String deksripsi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalInsert;

    private String userEdit;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalEdit;

    private String userDelete;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalDelete;

    @ManyToOne
    @JoinColumn(name = "id_lembaga")
    private Lembaga lembaga;

}