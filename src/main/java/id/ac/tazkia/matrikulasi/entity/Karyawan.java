package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
@Table
public class Karyawan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_lembaga")
    private Lembaga lembaga;

    @NotNull
    private String nik;

    @NotNull
    private String namaKaryawan;

    private String gelarDepan;

    private String gelarBelakang;

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    @ManyToOne
    @JoinColumn(name = "id_agama")
    private Agama agama;

    private String tempatLahir;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;

    private String alamatTinggal;

    private String kelurahan;
    private String kecamatan;
    private String kabupatenKota;
    private String provinsi;
    private String negara;
    private String kodePos;
    private String statusKewarganegaraan;

    @Enumerated(EnumType.STRING)
    private StatusRecord staff = StatusRecord.NONAKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord dosen = StatusRecord.NONAKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord pembina = StatusRecord.NONAKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String fileFoto;
    private String tanggalLahirString;

}