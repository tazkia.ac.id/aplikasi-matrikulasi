package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Kegiatan extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_matrikulasi")
    private Matrikulasi matrikulasi;

    private String kodeKegiatan;
    private String namaKegiatan;
    private String deskripsi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
