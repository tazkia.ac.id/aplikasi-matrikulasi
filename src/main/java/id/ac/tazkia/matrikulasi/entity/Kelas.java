package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Kelas extends BaseEntity{

    @NotEmpty
    private String kodeKelas;

    @NotEmpty
    private String namaKelas;

    private String angkatan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

}