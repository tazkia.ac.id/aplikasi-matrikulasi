package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
public class KelasTlc extends BaseEntity {

    @NotEmpty
    private String kodeKelasTlc;

    @NotEmpty
    private String namaKelasTlc;

    private String angkatan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

}