package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Data @Entity
@Table
public class Kewajiban extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_matrikulasi")
    private Matrikulasi matrikulasi;

    @ManyToOne
    @JoinColumn(name = "id_jenis_kewajiban")
    private JenisKewajiban jenisKewajiban;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiBerlaku;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date waktuMulai;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date waktuSelesai;

    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    private BigDecimal bobotNilai;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private TipeCheckIn tipeCheckIn;

    private String deskripsi;
    private String tanggalMulaiBerlakuString;
}