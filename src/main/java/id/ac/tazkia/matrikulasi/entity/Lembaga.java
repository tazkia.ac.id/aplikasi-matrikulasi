package id.ac.tazkia.matrikulasi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Data
@Table
public class Lembaga extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String kodeLembaga;

    private String namaLembaga;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalInsert;

    private String userEdit;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalEdit;

    private String userDelete;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalDelete;


}
