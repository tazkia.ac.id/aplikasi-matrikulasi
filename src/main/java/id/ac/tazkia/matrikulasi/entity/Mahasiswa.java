package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;


@Data
@Entity
@Table
public class Mahasiswa extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_lembaga")
    private Lembaga lembaga;

    private String angkatan;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String nim;
    private String nama;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusMatrikulasi = StatusRecord.AKTIF;

    //id program
    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    @ManyToOne
    @JoinColumn(name = "id_agama")
    private Agama agama;

    private String tempatLahir;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;

    //id kelurahan
    //id kecamatan
    //id kota kabupaten
    //id propinsi
    //id negara

    private String kewarganegaraan;
    private String nik;
    private String nisn;
    private String namaJalan;
    private String rt;
    private String rw;
    private String namaDusun;
    private String kodePos;
    private String jenisTinggal;
    private String alatTransportasi;
    private String teleponRumah;
    private String teleponSeluler;
    private String emailPribadi;
    private String emailTazkia;
    private String fileFoto;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String tanggalLahirString;

    @Transient
    private StatusPresensi statusPresensi;

}