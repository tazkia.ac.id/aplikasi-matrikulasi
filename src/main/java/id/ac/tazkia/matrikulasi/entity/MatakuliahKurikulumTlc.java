package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class MatakuliahKurikulumTlc {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_kurikulum_tlc")
    private KurikulumTlc kurikulumTlc;

    @ManyToOne
    @JoinColumn(name = "id_matakuliah_tlc")
    private MatakuliahTlc matakuliahTlc;

    @NotNull
    private Integer semester;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
