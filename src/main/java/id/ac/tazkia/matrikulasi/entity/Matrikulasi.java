package id.ac.tazkia.matrikulasi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.aspectj.lang.annotation.DeclareAnnotation;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table
@Data
public class Matrikulasi extends BaseEntity {

    private String namaMatrikulasi;

    private String alamat;

    private String kodepos;

    private String email;

    private String telepon;

    private String fax;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalUpdate = new Date();

    @ManyToOne
    @JoinColumn(name = "id_kampus")
    private Kampus kampus;


}
