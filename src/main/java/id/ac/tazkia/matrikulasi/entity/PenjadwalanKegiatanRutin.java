package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.*;

@Data
@Entity
public class PenjadwalanKegiatanRutin extends BaseEntity {

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    @ManyToOne
    @JoinColumn(name = "id_kegiatan")
    private Kegiatan kegiatan;

    @ManyToOne
    @JoinColumn(name = "id_ruangan")
    private Ruangan ruangan;

    private String deskripsiKegiatan;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalPelaksanaan;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date jamMulai;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String tanggalPelaksanaanString;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "penjadwalanKegiatanRutin",orphanRemoval = true)
    private List<PresensiMahasiswaKegiatanRutin> presensiMahasiswaKegiatanRutins = new ArrayList<>();

    @Transient
    private Set<Karyawan> pembinas = new HashSet<>();

    public StatusPresensi getMahasiswaPresensi(Mahasiswa mahasiswa) {
        for (PresensiMahasiswaKegiatanRutin prk : this.presensiMahasiswaKegiatanRutins){
            if (prk.getMahasiswa().equals(mahasiswa)) return prk.getStatusPresensi();
        }
        return null;
    }

}
