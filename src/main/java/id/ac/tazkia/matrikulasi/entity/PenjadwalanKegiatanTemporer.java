package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Data
@Entity
public class PenjadwalanKegiatanTemporer extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_kegiatan")
    private Kegiatan kegiatan;

    @ManyToOne
    @JoinColumn(name = "id_ruangan")
    private Ruangan ruangan;

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    private String deksripsi;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalKegiatan;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date jamMulai;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date jamSelesai;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String tanggalKegiatanString;

    @Transient
    private Set<Karyawan> pembina = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kegiatanTemporer",orphanRemoval = true)
    private List<PresensiMahasiswaKegiatanTemporer> absenMahasiswas = new ArrayList<>();

    public StatusPresensi getMahasiswaAttendance (Mahasiswa mahasiswa) {
        for (PresensiMahasiswaKegiatanTemporer am : this.absenMahasiswas) {
            if (am.getMahasiswa().equals(mahasiswa)) return am.getStatusPresensi();
        }
        return null;
    }
}