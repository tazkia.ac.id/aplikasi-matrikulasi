package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlotingKelas extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "id_kelas")
    private Kelas kelas;

    @ManyToOne
    @JoinColumn(name = "id_mahasiswa")
    private Mahasiswa mahasiswa;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


}
