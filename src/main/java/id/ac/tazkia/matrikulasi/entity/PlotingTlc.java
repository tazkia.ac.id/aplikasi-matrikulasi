package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
public class PlotingTlc extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_kelas_tlc")
    private KelasTlc kelasTlc;

    @ManyToOne
    @JoinColumn(name = "id_mahasiswa")
    private Mahasiswa mahasiswa;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
