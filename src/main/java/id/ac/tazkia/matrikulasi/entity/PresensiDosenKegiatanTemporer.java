package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class PresensiDosenKegiatanTemporer extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_penjadwalan")
    private PenjadwalanKegiatanTemporer kegiatanTemporer;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date waktuMulai;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date waktuSelesai;

    private String pemateri;
    private String beritaAcara;

    @Enumerated(EnumType.STRING)
    private StatusPresensi statusPresensi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}