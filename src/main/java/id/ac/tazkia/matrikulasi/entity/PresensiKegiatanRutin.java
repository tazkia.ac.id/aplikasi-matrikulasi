package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class PresensiKegiatanRutin extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_penjadwalan")
    private PenjadwalanKegiatanRutin penjadwalanKegiatanRutin;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    private Date waktuMasuk;
    private Date waktuKeluar;

    private String pemateri;
    private String beritaAcara;

    @Enumerated(EnumType.STRING)
    private StatusPresensi statusPresesnsi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


}
