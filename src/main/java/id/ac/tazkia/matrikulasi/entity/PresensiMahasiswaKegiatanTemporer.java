package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
public class PresensiMahasiswaKegiatanTemporer extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_kegiatan")
    private PenjadwalanKegiatanTemporer kegiatanTemporer;

    @ManyToOne
    @JoinColumn(name = "id_mahasiswa")
    private Mahasiswa mahasiswa;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date waktuMulai;

    @DateTimeFormat(pattern = "HH:mm")
    @Temporal(TemporalType.TIME)
    private Date waktuSelesai;

    @Enumerated(EnumType.STRING)
    private StatusPresensi statusPresensi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}