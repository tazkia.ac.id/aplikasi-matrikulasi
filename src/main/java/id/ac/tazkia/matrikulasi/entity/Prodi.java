package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Prodi extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_fakultas")
    private Fakultas fakultas;

//    @ManyToOne
//    @JoinColumn(name = "id_jenjang")
//    private Jenjang jenjang;

    private String kodeProdi;
    private String namaProdi;
    private String keterangan;
    private String warna;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}