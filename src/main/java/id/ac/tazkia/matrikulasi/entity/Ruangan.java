package id.ac.tazkia.matrikulasi.entity;

import lombok.Getter;
import lombok.Setter;

import lombok.Data;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@Data
public class Ruangan extends BaseEntity {

    private int letakLantai;
    private String kodeRuang;
    private String namaRuang;
    private int kapasitas;

    private String userInsert;

    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalInsert = new Date();

    private String userEdit;

    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalEdit = new Date();

    private String userDelete;

    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalDelete =  new Date();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_jenis_ruang")
    private JenisRuang jenisRuang;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_gedung")
    private Gedung gedung;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}