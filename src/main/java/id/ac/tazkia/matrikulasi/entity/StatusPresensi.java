package id.ac.tazkia.matrikulasi.entity;

public enum StatusPresensi {
    HADIR, MANGKIR, IZIN, SAKIT
}
