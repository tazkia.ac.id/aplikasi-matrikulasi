package id.ac.tazkia.matrikulasi.entity;

public enum StatusRecord {

    AKTIF, NONAKTIF, HAPUS, CUTI, KELUAR, MENGUNDURKAN_DIRI

}
