package id.ac.tazkia.matrikulasi.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Table
@Entity
public class TahunAkademikTlc extends BaseEntity {

    //id tahun akademik

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_tahun_akademik", nullable = false)
    private TahunAkademik tahunAkademik;

    @ManyToOne
    @JoinColumn(name = "id_matrikulasi")
    private Matrikulasi matrikulasi;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiSemesterTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiPenagihanTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiPenagihanTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiPembayaranTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiPembayaranTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiPlotingDosenTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiPlotingDosenTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiPenjadwalanTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiPenjadwalanTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiKrsTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiKrsTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiKuliahTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiKuliahTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiUtsTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiUtsTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiUasTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiUasTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalMulaiPenilaianTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiPenilaianTlc;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSelesaiSemesterTlc;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String tanggalMulaiSemesterTlcString;
    private String tanggalMulaiPenagihanTlcString;
    private String tanggalSelesaiPenagihanTlcString;
    private String tanggalMulaiPembayaranTlcString;
    private String tanggalSelesaiPembayaranTlcString;
    private String tanggalMulaiPlotingDosenTlcString;
    private String tanggalSelesaiPlotingDosenTlcString;
    private String tanggalMulaiPenjadwalanTlcString;
    private String tanggalSelesaiPenjadwalanTlcString;
    private String tanggalMulaiKrsTlcString;
    private String tanggalSelesaiKrsTlcString;
    private String tanggalMulaiKuliahTlcString;
    private String tanggalSelesaiKuliahTlcString;
    private String tanggalMulaiUtsTlcString;
    private String tanggalSelesaiUtsTlcString;
    private String tanggalMulaiUasTlcString;
    private String tanggalSelesaiUasTlcString;
    private String tanggalMulaiPenilaianTlcString;
    private String tanggalSelesaiPenilaianTlcString;
    private String tanggalSelesaiSemesterTlcString;

}

