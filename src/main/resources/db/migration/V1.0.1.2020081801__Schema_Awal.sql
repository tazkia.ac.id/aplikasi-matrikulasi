-- CREATE TABLE `agama` (
--   `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `agama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `status` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--
-- CREATE TABLE `kampus` (
--   `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
--   `deksripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `kode_kampus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `nama_kampus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tanggal_delete` datetime DEFAULT NULL,
--   `tanggal_edit` datetime DEFAULT NULL,
--   `tanggal_insert` datetime DEFAULT NULL,
--   `user_delete` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `user_edit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `user_insert` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `id_lembaga` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
--   PRIMARY KEY (`id`),
--   KEY `FKn1k1xqcfqc9hl4i80m6l2d3` (`id_lembaga`),
--   CONSTRAINT `FKn1k1xqcfqc9hl4i80m6l2d3` FOREIGN KEY (`id_lembaga`) REFERENCES `lembaga` (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--
-- CREATE TABLE `karyawan` (
--   `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `id_lembaga` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `nik` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `nama_karyawan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `gelar_depan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `gelar_belakang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `jenis_kelamin` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `id_agama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tempat_lahir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tanggal_lahir` date DEFAULT NULL,
--   `alamat_tinggal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `kelurahan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `kecamatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `kabupaten_kota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `propinsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `negara` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `status_kewarganegaraan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `staff` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `dosen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `pembina` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `file_foto` longtext COLLATE utf8_unicode_ci,
--   `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--
-- CREATE TABLE `lembaga` (
--   `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
--   `kode_lembaga` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `nama_lembaga` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tanggal_delete` datetime DEFAULT NULL,
--   `tanggal_edit` datetime DEFAULT NULL,
--   `tanggal_insert` datetime DEFAULT NULL,
--   `user_delete` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `user_edit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `user_insert` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--
-- CREATE TABLE `matrikulasi` (
--   `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
--   `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `kodepos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `nama_matrikulasi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tanggal_update` datetime DEFAULT NULL,
--   `telepon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `user_update` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `id_kampus` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
--   PRIMARY KEY (`id`),
--   KEY `FKe816525v44bhlqc1o70a33ua4` (`id_kampus`),
--   CONSTRAINT `FKe816525v44bhlqc1o70a33ua4` FOREIGN KEY (`id_kampus`) REFERENCES `kampus` (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--
--
-- CREATE TABLE `pejabat_matrikulasi` (
--   `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `id_karyawan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `id_matrikulasi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tanggal_mulai` date DEFAULT NULL,
--   `tanggal_selesai` date DEFAULT NULL,
--   `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tanggal_mulai_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `tanggal_selesai_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--  create table agama (id varchar(255) not null, agama varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
--  create table fakultas (id varchar(36) not null, keterangan varchar(255), kode_fakultas varchar(255), nama_fakultas varchar(255), status varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
--  create table gedung (id varchar(36) not null, jumlah_lantai integer not null, nama_gedung varchar(255), status varchar(5) not null, tanggal_delete datetime, tanggal_edit datetime, tanggal_insert datetime, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_matrikulasi varchar(36) not null, primary key (id)) engine=InnoDB;
--  create table jenis_kewajiban (id varchar(36) not null, kode_jenis_kewajiban varchar(255), nama_jenis_kewajiban varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
--  create table jenis_ruang (id varchar(36) not null, jenis_ruangan varchar(255), kode_jenis_ruangan varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
--  create table kampus (id varchar(255) not null, deksripsi varchar(255), kode_kampus varchar(255), nama_kampus varchar(255), status varchar(255), tanggal_delete datetime not null, tanggal_edit datetime not null, tanggal_insert datetime not null, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
--  create table karyawan (id varchar(255) not null, alamat_tinggal varchar(255), gelar_belakang varchar(255), gelar_depan varchar(255), jenis_kelamin varchar(255), nama_karyawan varchar(255) not null, nik varchar(255) not null, status varchar(255), tanggal_lahir date, tempat_lahir varchar(255), id_agama varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
--  create table kegiatan (id varchar(36) not null, deskripsi varchar(255), kode_kegiatan varchar(255), nama_kegiatan varchar(255), status varchar(255), id_matrikulasi varchar(36), primary key (id)) engine=InnoDB;
--  create table kewajiban (id varchar(36) not null, bobot_nilai decimal(19,2), deskripsi varchar(255), status varchar(255), tanggal_mulai_berlaku datetime, tipe_check_in varchar(255), waktu_mulai time, waktu_selesai time, id_jenis_kewajiban varchar(36), id_matrikulasi varchar(36), primary key (id)) engine=InnoDB;
--  create table lembaga (id varchar(36) not null, kode_lembaga varchar(255), nama_lembaga varchar(255), status varchar(255), tanggal_delete datetime not null, tanggal_edit datetime not null, tanggal_insert datetime not null, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), primary key (id)) engine=InnoDB;
--  create table mahasiswa (id varchar(36) not null, alat_transportasi varchar(255), angkatan varchar(255), email_pribadi varchar(255), email_tazkia varchar(255), jenis_kelamin varchar(255), jenis_tinggal varchar(255), kewarganegaraan varchar(255), kode_pos varchar(255), nama varchar(255), nama_dusun varchar(255), nama_jalan varchar(255), nik varchar(255), nim varchar(255), nisn varchar(255), rt varchar(255), rw varchar(255), status varchar(255), status_aktif varchar(255), status_matrikulasi varchar(255), tanggal_lahir date, telepon_rumah varchar(255), telepon_seluler varchar(255), tempat_lahir varchar(255), id_agama varchar(255), id_lembaga varchar(36), id_prodi varchar(36), primary key (id)) engine=InnoDB;
--  create table matrikulasi (id varchar(36) not null, alamat varchar(255), email varchar(255), fax varchar(255), kodepos varchar(255), nama_matrikulasi varchar(255), status varchar(255), tanggal_update datetime not null, telepon varchar(255), user_update varchar(255), id_kampus varchar(255), primary key (id)) engine=InnoDB;
--  create table pejabat_matrikulasi (id varchar(255) not null, status varchar(255), tanggal_mulai DATE, tanggal_mulai_string varchar(255), tanggal_selesai DATE, tanggal_selesai_string varchar(255), id_karyawan varchar(255), id_matrikulasi varchar(36), primary key (id)) engine=InnoDB;
--  create table ploting_pembina (id varchar(36) not null, status integer, waktu_ploting DATE not null, id_karyawan varchar(255), id_mahasiswa varchar(36), primary key (id)) engine=InnoDB;
--  create table prodi (id varchar(36) not null, keterangan varchar(255), kode_prodi varchar(255), nama_prodi varchar(255), status varchar(255), warna varchar(255), id_fakultas varchar(36), primary key (id)) engine=InnoDB;
--  create table ruangan (id varchar(36) not null, kapasitas integer not null, kode_ruang varchar(255), letak_lantai integer not null, nama_ruang varchar(255), status varchar(255), tanggal_delete datetime, tanggal_edit datetime, tanggal_insert datetime, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_gedung varchar(36) not null, id_jenis_ruang varchar(36), primary key (id)) engine=InnoDB;
--  create table tahun_akademik_matrikulasi (id varchar(36) not null, status varchar(255), tanggal_mulai_krs date, tanggal_mulai_kuliah date, tanggal_mulai_pembayaran date, tanggal_mulai_penagihan date, tanggal_mulai_penilaian date, tanggal_mulai_penjadwalan date, tanggal_mulai_ploting_dosen date, tanggal_mulai_semester date, tanggal_mulai_uas date, tanggal_mulai_uts date, tanggal_selesai_krs date, tanggal_selesai_kuliah date, tanggal_selesai_pembayaran date, tanggal_selesai_penagihan date, tanggal_selesai_penilaian date, tanggal_selesai_penjadwalan date, tanggal_selesai_ploting_dosen date, tanggal_selesai_semester date, tanggal_selesai_uas date, tanggal_selesai_uts date, primary key (id)) engine=InnoDB;
--  create table tahun_akademiktlc (id varchar(36) not null, status varchar(255), tanggal_mulai_penagihan date, tanggal_mulai_semester date, tanggal_selesai_penagihan date, tanggalmulai_krs date, tanggalmulai_kuliah date, tanggalmulai_pembayaran date, tanggalmulai_penilaian date, tanggalmulai_penjadwalan date, tanggalmulai_ploting_dosen date, tanggalmulai_uas date, tanggalmulai_uts date, tanggalselesai_krs date, tanggalselesai_kuliah date, tanggalselesai_pembayaran date, tanggalselesai_penilaian date, tanggalselesai_penjadwalan date, tanggalselesai_ploting_dosen date, tanggalselesai_semester date, tanggalselesai_uas date, tanggalselesai_uts date, primary key (id)) engine=InnoDB;
--  alter table fakultas add constraint FK87p2ljd0lnt5r1wij6qb3cqkv foreign key (id_lembaga) references lembaga (id);
--  alter table gedung add constraint FK8tfwvhsflm9bekyecgjgxx2mx foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table kampus add constraint FKn1k1xqcfqc9hl4i80m6l2d3 foreign key (id_lembaga) references lembaga (id);
--  alter table karyawan add constraint FKe2e33pmfvf308uq12otxweai3 foreign key (id_agama) references agama (id);
--  alter table karyawan add constraint FKirfylg6nqwlys9swr8akbtv9v foreign key (id_lembaga) references lembaga (id);
--  alter table kegiatan add constraint FKbd8f8qqjbimc2be0vdu9gf9va foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table kewajiban add constraint FK1t77floarr5edr7ri1cj5e25p foreign key (id_jenis_kewajiban) references jenis_kewajiban (id);
--  alter table kewajiban add constraint FKkrnlnx92drrh0rfbe1j9nmfwy foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table mahasiswa add constraint FKqwnwjp1bl2rh1v9itr6xe7qra foreign key (id_agama) references agama (id);
--  alter table mahasiswa add constraint FKnv0xvmw0soxcyx2drc7llhje4 foreign key (id_lembaga) references lembaga (id);
--  alter table mahasiswa add constraint FKedsc31p4rrx07r2ptx3gpwbth foreign key (id_prodi) references prodi (id);
--  alter table matrikulasi add constraint FKe816525v44bhlqc1o70a33ua4 foreign key (id_kampus) references kampus (id);
--  alter table pejabat_matrikulasi add constraint FK3wod4idv75fjyvqxf8ixscfqb foreign key (id_karyawan) references karyawan (id);
--  alter table pejabat_matrikulasi add constraint FKt4hm9dpkklnp3hu68oipltb5q foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table ploting_pembina add constraint FKsah2lt5qd619h3wr9xglccqwf foreign key (id_karyawan) references karyawan (id);
--  alter table ploting_pembina add constraint FKbad3jv3gck4ov681mqgwe1xu7 foreign key (id_mahasiswa) references mahasiswa (id);
--  alter table prodi add constraint FKm5yuxadgft6ky9y45mivpp7ic foreign key (id_fakultas) references fakultas (id);
--  alter table ruangan add constraint FK64lkr6kmwtihnwvbbndnpfl3m foreign key (id_gedung) references gedung (id);
--  alter table ruangan add constraint FKnxk95w69cg93hm4s76rq6k79j foreign key (id_jenis_ruang) references jenis_ruang (id);

--  create table agama (id varchar(255) not null, agama varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
--  create table fakultas (id varchar(36) not null, keterangan varchar(255), kode_fakultas varchar(255), nama_fakultas varchar(255), status varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
--  create table gedung (id varchar(36) not null, jumlah_lantai integer not null, nama_gedung varchar(255), status varchar(5) not null, tanggal_delete datetime, tanggal_edit datetime, tanggal_insert datetime, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_matrikulasi varchar(36) not null, primary key (id)) engine=InnoDB;
--  create table jenis_kewajiban (id varchar(36) not null, kode_jenis_kewajiban varchar(255), nama_jenis_kewajiban varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
--  create table jenis_ruang (id varchar(36) not null, jenis_ruangan varchar(255), kode_jenis_ruangan varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
--  create table kampus (id varchar(255) not null, deksripsi varchar(255), kode_kampus varchar(255), nama_kampus varchar(255), status varchar(255), tanggal_delete datetime not null, tanggal_edit datetime not null, tanggal_insert datetime not null, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
--  create table karyawan (id varchar(255) not null, alamat_tinggal varchar(255), gelar_belakang varchar(255), gelar_depan varchar(255), jenis_kelamin varchar(255), kode_pos varchar(255), nama_karyawan varchar(255) not null, nik varchar(255) not null, status varchar(255), tanggal_lahir date, tempat_lahir varchar(255), id_agama varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
--  create table kegiatan (id varchar(36) not null, deskripsi varchar(255), kode_kegiatan varchar(255), nama_kegiatan varchar(255), status varchar(255), id_matrikulasi varchar(36), primary key (id)) engine=InnoDB;
--  create table kewajiban (id varchar(36) not null, bobot_nilai decimal(19,2), deskripsi varchar(255), status varchar(255), tanggal_mulai_berlaku datetime, tipe_check_in varchar(255), waktu_mulai time, waktu_selesai time, id_jenis_kewajiban varchar(36), id_matrikulasi varchar(36), primary key (id)) engine=InnoDB;
--  create table lembaga (id varchar(36) not null, kode_lembaga varchar(255), nama_lembaga varchar(255), status varchar(255), tanggal_delete datetime not null, tanggal_edit datetime not null, tanggal_insert datetime not null, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), primary key (id)) engine=InnoDB;
--  create table mahasiswa (id varchar(36) not null, alat_transportasi varchar(255), angkatan varchar(255), email_pribadi varchar(255), email_tazkia varchar(255), jenis_kelamin varchar(255), jenis_tinggal varchar(255), kewarganegaraan varchar(255), kode_pos varchar(255), nama varchar(255), nama_dusun varchar(255), nama_jalan varchar(255), nik varchar(255), nim varchar(255), nisn varchar(255), rt varchar(255), rw varchar(255), status varchar(255), status_aktif varchar(255), status_matrikulasi varchar(255), tanggal_lahir date, telepon_rumah varchar(255), telepon_seluler varchar(255), tempat_lahir varchar(255), id_agama varchar(255), id_lembaga varchar(36), id_prodi varchar(36), primary key (id)) engine=InnoDB;
--  create table matrikulasi (id varchar(36) not null, alamat varchar(255), email varchar(255), fax varchar(255), kodepos varchar(255), nama_matrikulasi varchar(255), status varchar(255), tanggal_update datetime not null, telepon varchar(255), user_update varchar(255), id_kampus varchar(255), primary key (id)) engine=InnoDB;
--  create table pejabat_matrikulasi (id varchar(255) not null, status varchar(255), tanggal_mulai DATE, tanggal_mulai_string varchar(255), tanggal_selesai DATE, tanggal_selesai_string varchar(255), id_karyawan varchar(255), id_matrikulasi varchar(36), primary key (id)) engine=InnoDB;
--  create table ploting_pembina (id varchar(36) not null, status integer, waktu_ploting DATE not null, id_karyawan varchar(255), id_mahasiswa varchar(36), primary key (id)) engine=InnoDB;
--  create table prodi (id varchar(36) not null, keterangan varchar(255), kode_prodi varchar(255), nama_prodi varchar(255), status varchar(255), warna varchar(255), id_fakultas varchar(36), primary key (id)) engine=InnoDB;
--  create table ruangan (id varchar(36) not null, kapasitas integer not null, kode_ruang varchar(255), letak_lantai integer not null, nama_ruang varchar(255), status varchar(255), tanggal_delete datetime, tanggal_edit datetime, tanggal_insert datetime, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_gedung varchar(36) not null, id_jenis_ruang varchar(36), primary key (id)) engine=InnoDB;
--  create table tahun_akademik_matrikulasi (id varchar(36) not null, status varchar(255), tanggal_mulai_krs date, tanggal_mulai_kuliah date, tanggal_mulai_pembayaran date, tanggal_mulai_penagihan date, tanggal_mulai_penilaian date, tanggal_mulai_penjadwalan date, tanggal_mulai_ploting_dosen date, tanggal_mulai_semester date, tanggal_mulai_uas date, tanggal_mulai_uts date, tanggal_selesai_krs date, tanggal_selesai_kuliah date, tanggal_selesai_pembayaran date, tanggal_selesai_penagihan date, tanggal_selesai_penilaian date, tanggal_selesai_penjadwalan date, tanggal_selesai_ploting_dosen date, tanggal_selesai_semester date, tanggal_selesai_uas date, tanggal_selesai_uts date, primary key (id)) engine=InnoDB;
--  create table tahun_akademiktlc (id varchar(36) not null, status varchar(255), tanggal_mulai_penagihan date, tanggal_mulai_semester date, tanggal_selesai_penagihan date, tanggalmulai_krs date, tanggalmulai_kuliah date, tanggalmulai_pembayaran date, tanggalmulai_penilaian date, tanggalmulai_penjadwalan date, tanggalmulai_ploting_dosen date, tanggalmulai_uas date, tanggalmulai_uts date, tanggalselesai_krs date, tanggalselesai_kuliah date, tanggalselesai_pembayaran date, tanggalselesai_penilaian date, tanggalselesai_penjadwalan date, tanggalselesai_ploting_dosen date, tanggalselesai_semester date, tanggalselesai_uas date, tanggalselesai_uts date, primary key (id)) engine=InnoDB;
--  alter table fakultas add constraint FK87p2ljd0lnt5r1wij6qb3cqkv foreign key (id_lembaga) references lembaga (id);
--  alter table gedung add constraint FK8tfwvhsflm9bekyecgjgxx2mx foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table kampus add constraint FKn1k1xqcfqc9hl4i80m6l2d3 foreign key (id_lembaga) references lembaga (id);
--  alter table karyawan add constraint FKe2e33pmfvf308uq12otxweai3 foreign key (id_agama) references agama (id);
--  alter table karyawan add constraint FKirfylg6nqwlys9swr8akbtv9v foreign key (id_lembaga) references lembaga (id);
--  alter table kegiatan add constraint FKbd8f8qqjbimc2be0vdu9gf9va foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table kewajiban add constraint FK1t77floarr5edr7ri1cj5e25p foreign key (id_jenis_kewajiban) references jenis_kewajiban (id);
--  alter table kewajiban add constraint FKkrnlnx92drrh0rfbe1j9nmfwy foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table mahasiswa add constraint FKqwnwjp1bl2rh1v9itr6xe7qra foreign key (id_agama) references agama (id);
--  alter table mahasiswa add constraint FKnv0xvmw0soxcyx2drc7llhje4 foreign key (id_lembaga) references lembaga (id);
--  alter table mahasiswa add constraint FKedsc31p4rrx07r2ptx3gpwbth foreign key (id_prodi) references prodi (id);
--  alter table matrikulasi add constraint FKe816525v44bhlqc1o70a33ua4 foreign key (id_kampus) references kampus (id);
--  alter table pejabat_matrikulasi add constraint FK3wod4idv75fjyvqxf8ixscfqb foreign key (id_karyawan) references karyawan (id);
--  alter table pejabat_matrikulasi add constraint FKt4hm9dpkklnp3hu68oipltb5q foreign key (id_matrikulasi) references matrikulasi (id);
--  alter table ploting_pembina add constraint FKsah2lt5qd619h3wr9xglccqwf foreign key (id_karyawan) references karyawan (id);
--  alter table ploting_pembina add constraint FKbad3jv3gck4ov681mqgwe1xu7 foreign key (id_mahasiswa) references mahasiswa (id);
--  alter table prodi add constraint FKm5yuxadgft6ky9y45mivpp7ic foreign key (id_fakultas) references fakultas (id);
--  alter table ruangan add constraint FK64lkr6kmwtihnwvbbndnpfl3m foreign key (id_gedung) references gedung (id);
--  alter table ruangan add constraint FKnxk95w69cg93hm4s76rq6k79j foreign key (id_jenis_ruang) references jenis_ruang (id);
 create table agama (id varchar(255) not null, agama varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
 create table gedung (id varchar(36) not null, jumlah_lantai integer not null, nama_gedung varchar(255), status varchar(5) not null, tanggal_delete datetime, tanggal_edit datetime, tanggal_insert datetime, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_matrikulasi varchar(36) not null, primary key (id)) engine=InnoDB;
 create table jenis_ruang (id varchar(36) not null, jenis_ruangan varchar(255), kode_jenis_ruangan varchar(255), status varchar(255), primary key (id)) engine=InnoDB;
 create table kampus (id varchar(36) not null, deksripsi varchar(255), kode_kampus varchar(255), nama_kampus varchar(255), status varchar(255), tanggal_delete datetime not null, tanggal_edit datetime not null, tanggal_insert datetime not null, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
 create table karyawan (id varchar(255) not null, alamat_tinggal varchar(255), gelar_belakang varchar(255), gelar_depan varchar(255), jenis_kelamin varchar(255), nama_karyawan varchar(255) not null, nik varchar(255) not null, status varchar(255), tanggal_lahir date, tempat_lahir varchar(255), id_agama varchar(255), id_lembaga varchar(36), primary key (id)) engine=InnoDB;
 create table lembaga (id varchar(36) not null, kode_lembaga varchar(255), nama_lembaga varchar(255), status varchar(255), tanggal_delete datetime not null, tanggal_edit datetime not null, tanggal_insert datetime not null, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), primary key (id)) engine=InnoDB;
 create table matrikulasi (id varchar(36) not null, alamat varchar(255), email varchar(255), fax varchar(255), kodepos varchar(255), nama_matrikulasi varchar(255), status varchar(255), tanggal_update datetime not null, telepon varchar(255), user_update varchar(255), id_kampus varchar(36), primary key (id)) engine=InnoDB;
 create table pejabat_matrikulasi (id varchar(255) not null, status varchar(255), tanggal_mulai DATE, tanggal_mulai_string varchar(255), tanggal_selesai DATE, tanggal_selesai_string varchar(255), id_karyawan varchar(255), id_matrikulasi varchar(36), primary key (id)) engine=InnoDB;
 create table ruangan (id varchar(36) not null, kapasitas integer not null, kode_ruang varchar(255), letak_lantai integer not null, nama_ruang varchar(255), status varchar(255), tanggal_delete datetime, tanggal_edit datetime, tanggal_insert datetime, user_delete varchar(255), user_edit varchar(255), user_insert varchar(255), id_gedung varchar(36) not null, id_jenis_ruang varchar(36), primary key (id)) engine=InnoDB;
 alter table gedung add constraint FK8tfwvhsflm9bekyecgjgxx2mx foreign key (id_matrikulasi) references matrikulasi (id);
 alter table kampus add constraint FKn1k1xqcfqc9hl4i80m6l2d3 foreign key (id_lembaga) references lembaga (id);
 alter table karyawan add constraint FKe2e33pmfvf308uq12otxweai3 foreign key (id_agama) references agama (id);
 alter table karyawan add constraint FKirfylg6nqwlys9swr8akbtv9v foreign key (id_lembaga) references lembaga (id);
 alter table matrikulasi add constraint FKe816525v44bhlqc1o70a33ua4 foreign key (id_kampus) references kampus (id);
 alter table pejabat_matrikulasi add constraint FK3wod4idv75fjyvqxf8ixscfqb foreign key (id_karyawan) references karyawan (id);
 alter table pejabat_matrikulasi add constraint FKt4hm9dpkklnp3hu68oipltb5q foreign key (id_matrikulasi) references matrikulasi (id);
 alter table ruangan add constraint FK64lkr6kmwtihnwvbbndnpfl3m foreign key (id_gedung) references gedung (id);
 alter table ruangan add constraint FKnxk95w69cg93hm4s76rq6k79j foreign key (id_jenis_ruang) references jenis_ruang (id);
